const express = require('express');
const app = express();
const server = require('http').createServer(app);
const path = require('path');
const bodyParser = require("body-parser");
var session = require('express-session');
var request = require('request');
const bibtexParse = require('bibtex-parse');
const fs = require('fs');
const jquery = require('jquery');
const jsdom = require('jsdom');
const { JSDOM } = jsdom;

var options = {
    url: 'https://doi.org/10.1007/978-3-030-33220-4_19',
    method: 'GET',
    headers: {
        //Accept: 'application/x-bibtex; charset=utf-8'
        Accept: 'application/json'
    }
};

app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

var config="";

function callback(error, response, body) {
    if (!error && response.statusCode == 200) {
        //var obj = JSON.parse(bibtexParse.entries(body));
        //var bibtex = bibtexParse.entries(body);
        //console.log('Test: ' + bibtex.);
        var obj = JSON.parse(body);
        console.log(obj.author[2]);
        //console.log(body);
    }
}

//Funktion für den Login

function login(configstring,req) {
    req.session.loggedin = true;
    req.session.username = 'sa';
    req.session.connectionString = configstring;
    console.log(config);
    console.log('Connected!');
}

//Läd die Login-Seite

app.get('/', function (req,res) {
    res.sendFile(path.join(__dirname + '/login.html'));
});

//Logout

app.get('/logout', function(req,res) {
    req.session.destroy(err => {
        if(err) {
            console.log(err);
            req.sendFile(path.join(__dirname + '/start_site.html'));
        } else {
            res.redirect('/');
        }
    });
});

//Läd das Auswahlmenü

app.post('/start_site', function (req,res) {
    fs.readFile(__dirname + '/datenbank_IP.txt','utf8',function(err,data) {
        if (err) console.log(err);
        var config = 'mssql://' + req.body.username + ':' + req.body.password + '@' + data +':1433/Forschungsdatenbank';
        console.log(config);
        login(config,req);
        fs.readFile('auswertungsmenu_grundzustand.html','utf8', function(err, contents){
            let sql = require("mssql");
            const dom = new JSDOM(contents);
            const $ = jquery(dom.window);
            var pool = new sql.ConnectionPool(req.session.connectionString);
    
            pool.connect(err => {
                if(err) {
                    console.log(err);
                    res.send(err);
                    sql.close;
                } else {
                    let sqlRequest = new sql.Request(pool);
                    let sqlQuery = "select org_id,name_der_organisationseinheit from organisationseinheit";
                    
                    sqlRequest.query(sqlQuery).then(data => {
    
                        if (err) {
                            console.log(err);
                        } else {
                            console.table(data.recordset);
                            for (let j = 0;j<data.recordset.length;j++) {
                                $("#org_name").append($('<option>', {
                                    value: data.recordset[j].org_id,
                                    text: data.recordset[j].name_der_organisationseinheit
                                }));
                            }
                            fs.writeFile('auswertungsmenu.html', dom.serialize(), err=>{
                                console.log('done');
                                res.sendFile(path.join(__dirname + '/auswertungsmenu.html'));
                            });
                        }
    
                        sql.close; 
                    });
                }
            })
        });
    });
});


app.use(express.static(__dirname+'/public'));

//Läd und erstellt die Auswertungsseite

app.post('/auswertung',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            
            //abgeschlossene Promotionsverfahren
            let sqlQuery = "select count(*) abgeschlossene_Promotionsverfahren from abgeschlossene_Promotionsverfahren left join Promotionsverfahren on abgeschlossene_Promotionsverfahren.pro_id=Promotionsverfahren.pro_id left join Professoren on Promotionsverfahren.person_id_prof=Professoren.person_id left join Person on Professoren.person_id=person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id";
            sqlQuery = sqlQuery + " where organisationseinheit.org_id=" + req.body.org_name;
            if(req.body.date_begin!=""){
                sqlQuery = sqlQuery + " and datum_abschluss>='" + req.body.date_begin + "'";
            }
            if(req.body.date_end!=""){
                sqlQuery = sqlQuery + " and datum_abschluss<='" + req.body.date_end + "'";
            }  
            sqlQuery = sqlQuery + ";";

            //laufende Promotionsverfahren
            sqlQuery = sqlQuery + "select count(*) laufende_Promotionsverfahren from laufende_Promotionsverfahren left join Promotionsverfahren on laufende_Promotionsverfahren.pro_id=Promotionsverfahren.pro_id left join Professoren on Promotionsverfahren.person_id_prof=Professoren.person_id left join Person on Professoren.person_id=person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id";
            sqlQuery = sqlQuery + " where organisationseinheit.org_id=" + req.body.org_name + ";";
            
            //Graduierungsarbeit Bachelor ohne Kooperationspartner
            sqlQuery = sqlQuery + "select count(*) Bachelorarbeiten_ohne_Kooperationspartner from graduierungsarbeit left join Studenten on graduierungsarbeit.person_id=Studenten.person_id left join Person on Studenten.person_id=person.person_id left join gehoert_zu on person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id left join ist_koop_von_ga on graduierungsarbeit.grad_id=ist_koop_von_ga.grad_id where graduierungsarbeit.bachelor_master='Bachelor' and ist_koop_von_ga.grad_id is null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.body.org_name;
            if(req.body.date_begin!=""){
                sqlQuery = sqlQuery + " and datum_abschluss>='" + req.body.date_begin + "'";
            }
            if(req.body.date_end!=""){
                sqlQuery = sqlQuery + " and datum_abschluss<='" + req.body.date_end + "'";
            }  
            sqlQuery = sqlQuery + ";";

            //Graduierungsarbeit Bachelor mit Kooperationspartner
            sqlQuery = sqlQuery + "select count(*) Bachelorarbeiten_mit_Kooperationspartner from graduierungsarbeit left join Studenten on graduierungsarbeit.person_id=Studenten.person_id left join Person on Studenten.person_id=person.person_id left join gehoert_zu on person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id left join ist_koop_von_ga on graduierungsarbeit.grad_id=ist_koop_von_ga.grad_id where graduierungsarbeit.bachelor_master='Bachelor' and ist_koop_von_ga.grad_id is not null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.body.org_name;
            if(req.body.date_begin!=""){
                sqlQuery = sqlQuery + " and datum_abschluss>='" + req.body.date_begin + "'";
            }
            if(req.body.date_end!=""){
                sqlQuery = sqlQuery + " and datum_abschluss<='" + req.body.date_end + "'";
            }  
            sqlQuery = sqlQuery + ";";

            //Graduierungsarbeit Master ohne Kooperationspartner
            sqlQuery = sqlQuery + "select count(*) Masterarbeiten_ohne_Kooperationspartner from graduierungsarbeit left join Studenten on graduierungsarbeit.person_id=Studenten.person_id left join Person on Studenten.person_id=person.person_id left join gehoert_zu on person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id left join ist_koop_von_ga on graduierungsarbeit.grad_id=ist_koop_von_ga.grad_id where graduierungsarbeit.bachelor_master='Master' and ist_koop_von_ga.grad_id is null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.body.org_name;
            if(req.body.date_begin!=""){
                sqlQuery = sqlQuery + " and datum_abschluss>='" + req.body.date_begin + "'";
            }
            if(req.body.date_end!=""){
                sqlQuery = sqlQuery + " and datum_abschluss<='" + req.body.date_end + "'";
            }  
            sqlQuery = sqlQuery + ";";

            //Graduierungsarbeit Master mit Kooperationspartner
            sqlQuery = sqlQuery + "select count(*) Masterarbeiten_mit_Kooperationspartner from graduierungsarbeit left join Studenten on graduierungsarbeit.person_id=Studenten.person_id left join Person on Studenten.person_id=person.person_id left join gehoert_zu on person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id left join ist_koop_von_ga on graduierungsarbeit.grad_id=ist_koop_von_ga.grad_id where graduierungsarbeit.bachelor_master='Master' and ist_koop_von_ga.grad_id is not null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.body.org_name;
            if(req.body.date_begin!=""){
                sqlQuery = sqlQuery + " and datum_abschluss>='" + req.body.date_begin + "'";
            }
            if(req.body.date_end!=""){
                sqlQuery = sqlQuery + " and datum_abschluss<='" + req.body.date_end + "'";
            }  
            sqlQuery = sqlQuery + ";";

            //Wissenschaftliche Veröffentlichungen (Monografien/Sammelbände)
            sqlQuery = sqlQuery + "select count(*) Wissen_Veroeffentlichungen_M_S from Wissenschaftliche_Veroeffentlichung left join Monografien_Beitraege_in_Sammelbaenden on Wissenschaftliche_Veroeffentlichung.ver_id=Monografien_Beitraege_in_Sammelbaenden.ver_id left join ist_autor_von on Wissenschaftliche_Veroeffentlichung.ver_id=ist_autor_von.ver_id left join Person on ist_autor_von.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id where Monografien_Beitraege_in_Sammelbaenden.ver_id is not null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.body.org_name;
            if(req.body.date_begin!=""){
                let year = new Date(req.body.date_begin);
                year = year.getFullYear();
                sqlQuery = sqlQuery + " and jahr>='" + year + "'";
            }
            if(req.body.date_end!=""){
                let year = new Date(req.body.date_begin);
                year = year.getFullYear();
                sqlQuery = sqlQuery + " and jahr<='" + year + "'";
            }  
            sqlQuery = sqlQuery + ";";

            //Wissenschaftliche Veröffentlichungen (Fachbeiträge in wissen. Zeitschriften)
            sqlQuery = sqlQuery + "select count(*) Fachbeitraege_in_wissen_Zeitschriften from Wissenschaftliche_Veroeffentlichung left join Fachbeitraege_in_wissen_Zeitschriften on Wissenschaftliche_Veroeffentlichung.ver_id=Fachbeitraege_in_wissen_Zeitschriften.ver_id left join ist_autor_von on Wissenschaftliche_Veroeffentlichung.ver_id=ist_autor_von.ver_id left join Person on ist_autor_von.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id where Fachbeitraege_in_wissen_Zeitschriften.ver_id is not null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.body.org_name;
            if(req.body.date_begin!=""){
                let year = new Date(req.body.date_begin);
                year = year.getFullYear();
                sqlQuery = sqlQuery + " and jahr>='" + year + "'";
            }
            if(req.body.date_end!=""){
                let year = new Date(req.body.date_begin);
                year = year.getFullYear();
                sqlQuery = sqlQuery + " and jahr<='" + year + "'";
            }  
            sqlQuery = sqlQuery + ";";

            //Wissenschaftliche Veröffentlichungen (Kurzbeiträge)
            sqlQuery = sqlQuery + "select count(*) Kurzbeitraege from Wissenschaftliche_Veroeffentlichung left join kurzbeitraege on Wissenschaftliche_Veroeffentlichung.ver_id=kurzbeitraege.ver_id left join ist_autor_von on Wissenschaftliche_Veroeffentlichung.ver_id=ist_autor_von.ver_id left join Person on ist_autor_von.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id where kurzbeitraege.ver_id is not null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.body.org_name;
            if(req.body.date_begin!=""){
                let year = new Date(req.body.date_begin);
                year = year.getFullYear();
                sqlQuery = sqlQuery + " and jahr>='" + year + "'";
            }
            if(req.body.date_end!=""){
                let year = new Date(req.body.date_begin);
                year = year.getFullYear();
                sqlQuery = sqlQuery + " and jahr<='" + year + "'";
            }  
            sqlQuery = sqlQuery + ";";

            //Wissenschaftliche Veröffentlichungen (Veröffentlicht im Rahmen von wissen. Veranstaltungen)
            sqlQuery = sqlQuery + "select count(*) Ver_im_Rahmen_v_Veranst from Wissenschaftliche_Veroeffentlichung left join veroeffentlichungen_im_Rahmen_von_wissen_Veranstaltungen on Wissenschaftliche_Veroeffentlichung.ver_id=veroeffentlichungen_im_Rahmen_von_wissen_Veranstaltungen.ver_id left join ist_autor_von on Wissenschaftliche_Veroeffentlichung.ver_id=ist_autor_von.ver_id left join Person on ist_autor_von.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id where veroeffentlichungen_im_Rahmen_von_wissen_Veranstaltungen.ver_id is not null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.body.org_name;
            if(req.body.date_begin!=""){
                let year = new Date(req.body.date_begin);
                year = year.getFullYear();
                sqlQuery = sqlQuery + " and jahr>='" + year + "'";
            }
            if(req.body.date_end!=""){
                let year = new Date(req.body.date_begin);
                year = year.getFullYear();
                sqlQuery = sqlQuery + " and jahr<='" + year + "'";
            }  
            sqlQuery = sqlQuery + ";";

            //sonstige Veröffentichungen
            sqlQuery = sqlQuery + "select count(*) sonstige_Veroeffentlichungen from Wissenschaftliche_Veroeffentlichung left join Fachbeitraege_in_wissen_Zeitschriften on Wissenschaftliche_Veroeffentlichung.ver_id=Fachbeitraege_in_wissen_Zeitschriften.ver_id left join veroeffentlichungen_im_Rahmen_von_wissen_Veranstaltungen on Wissenschaftliche_Veroeffentlichung.ver_id=veroeffentlichungen_im_Rahmen_von_wissen_Veranstaltungen.ver_id left join kurzbeitraege on Wissenschaftliche_Veroeffentlichung.ver_id=kurzbeitraege.ver_id left join Monografien_Beitraege_in_Sammelbaenden on Wissenschaftliche_Veroeffentlichung.ver_id=Monografien_Beitraege_in_Sammelbaenden.ver_id left join ist_autor_von on Wissenschaftliche_Veroeffentlichung.ver_id=ist_autor_von.ver_id left join Person on ist_autor_von.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id where Fachbeitraege_in_wissen_Zeitschriften.ver_id is null and veroeffentlichungen_im_Rahmen_von_wissen_Veranstaltungen.ver_id is null and kurzbeitraege.ver_id is null and Monografien_Beitraege_in_Sammelbaenden.ver_id is null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.body.org_name;
            if(req.body.date_begin!=""){
                let year = new Date(req.body.date_begin);
                year = year.getFullYear();
                sqlQuery = sqlQuery + " and jahr>='" + year + "'";
            }
            if(req.body.date_end!=""){
                let year = new Date(req.body.date_begin);
                year = year.getFullYear();
                sqlQuery = sqlQuery + " and jahr<='" + year + "'";
            }  
            sqlQuery = sqlQuery + ";";

            //FuE-Projekte mit Kooperationspartner
            sqlQuery = sqlQuery + "select count(*) FuE_Projekte_mit_Kooperationspartner from fue_projekte left join Professoren on fue_projekte.person_id=Professoren.person_id left join Person on Professoren.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id left join ist_koop_von_proj on fue_projekte.fue_id=ist_koop_von_proj.fue_id where ist_koop_von_proj.fue_id is not null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.body.org_name;
            if(req.body.date_begin!=""){
                sqlQuery = sqlQuery + " and beginn>='" + req.body.date_begin + "'";
            }
            if(req.body.date_end!=""){
                sqlQuery = sqlQuery + " and beginn<='" + req.body.date_end + "'";
            }  
            sqlQuery = sqlQuery + ";";

            //FuE-Projekte ohne Kooperationspartner
            sqlQuery = sqlQuery + "select count(*) FuE_Projekte_ohne_Kooperationspartner from fue_projekte left join Professoren on fue_projekte.person_id=Professoren.person_id left join Person on Professoren.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id left join ist_koop_von_proj on fue_projekte.fue_id=ist_koop_von_proj.fue_id where ist_koop_von_proj.fue_id is null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.body.org_name;
            if(req.body.date_begin!=""){
                sqlQuery = sqlQuery + " and beginn>='" + req.body.date_begin + "'";
            }
            if(req.body.date_end!=""){
                sqlQuery = sqlQuery + " and beginn<='" + req.body.date_end + "'";
            }  
            sqlQuery = sqlQuery + ";";

            //Wissenschaftliche Vorträge auf wissen Veranstaltungen
            sqlQuery = sqlQuery + "select count(distinct wissenschaftliche_vortraege.vortr_id) Vortraege_auf_wissen_Veranstaltungen from wissenschaftliche_vortraege left join haelt on wissenschaftliche_vortraege.vortr_id=haelt.vortr_id left join Person on haelt.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id left join gehalten_im_Rahmen_von on wissenschaftliche_vortraege.vortr_id=gehalten_im_Rahmen_von.vortr_id where gehalten_im_Rahmen_von.vortr_id is not null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.body.org_name;
            if(req.body.date_begin!=""){
                sqlQuery = sqlQuery + " and datum>='" + req.body.date_begin + "'";
            }
            if(req.body.date_end!=""){
                sqlQuery = sqlQuery + " and datum<='" + req.body.date_end + "'";
            }  
            sqlQuery = sqlQuery + ";";

            //Wissenschaftliche Vorträge sonstige
            sqlQuery = sqlQuery + "select count(distinct wissenschaftliche_vortraege.vortr_id) sonstige_Vortraege from wissenschaftliche_vortraege left join haelt on wissenschaftliche_vortraege.vortr_id=haelt.vortr_id left join Person on haelt.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id left join gehalten_im_Rahmen_von on wissenschaftliche_vortraege.vortr_id=gehalten_im_Rahmen_von.vortr_id where gehalten_im_Rahmen_von.vortr_id is null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.body.org_name;
            if(req.body.date_begin!=""){
                sqlQuery = sqlQuery + " and datum>='" + req.body.date_begin + "'";
            }
            if(req.body.date_end!=""){
                sqlQuery = sqlQuery + " and datum<='" + req.body.date_end + "'";
            }  
            sqlQuery = sqlQuery + ";";

            //Existenzgründung
            sqlQuery = sqlQuery + "select count(distinct Existenzgruendung.ex_id) Existenzgruendungen from Existenzgruendung left join gruendet_aus on Existenzgruendung.ex_id=gruendet_aus.ex_id left join Person on gruendet_aus.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id";
            sqlQuery = sqlQuery + " where organisationseinheit.org_id=" + req.body.org_name;
            if(req.body.date_begin!=""){
                sqlQuery = sqlQuery + " and gruendungsdatum>='" + req.body.date_begin + "'";
            }
            if(req.body.date_end!=""){
                sqlQuery = sqlQuery + " and gruendungsdatum<='" + req.body.date_end + "'";
            }  
            sqlQuery = sqlQuery + ";";

            //Anmeldung von Schutzrechten
            sqlQuery = sqlQuery + "select count(*) Anmeldung_Schutzrechte from Schutzrechte left join Person on Schutzrechte.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id";
            sqlQuery = sqlQuery + " where organisationseinheit.org_id=" + req.body.org_name;
            if(req.body.date_begin!=""){
                sqlQuery = sqlQuery + " and datum_der_anmeldung>='" + req.body.date_begin + "'";
            }
            if(req.body.date_end!=""){
                sqlQuery = sqlQuery + " and datum_der_anmeldung<='" + req.body.date_end + "'";
            }  
            sqlQuery = sqlQuery + ";";

            //Wissenschaftliche Veranstaltungen

            //Auszeichnungen und Forschungspreise (Inland)
            sqlQuery = sqlQuery + "select count(*) Auszeichnungen_inland from auszeichnung left join Person on auszeichnung.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id where auszeichnung.land='Deutschland'";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.body.org_name;
            if(req.body.date_begin!=""){
                sqlQuery = sqlQuery + " and datum>='" + req.body.date_begin + "'";
            }
            if(req.body.date_end!=""){
                sqlQuery = sqlQuery + " and datum<='" + req.body.date_end + "'";
            }  
            sqlQuery = sqlQuery + ";";

            //Auszeichnungen und Forschungspreise (Ausland)
            sqlQuery = sqlQuery + "select count(*) Auszeichnungen_ausland from auszeichnung left join Person on auszeichnung.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id where auszeichnung.land!='Deutschland'";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.body.org_name;
            if(req.body.date_begin!=""){
                sqlQuery = sqlQuery + " and datum>='" + req.body.date_begin + "'";
            }
            if(req.body.date_end!=""){
                sqlQuery = sqlQuery + " and datum<='" + req.body.date_end + "'";
            }  
            sqlQuery = sqlQuery + ";";

            //Gremientätigkeit im Inland
            sqlQuery = sqlQuery + "select count(*) Gremientaetigkeit_inland from Gremientaetigkeit left join Professoren on Gremientaetigkeit.person_id=Professoren.person_id left join Person on Professoren.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id where Gremientaetigkeit.land='Deutschland'";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.body.org_name;
            if(req.body.date_begin!=""){
                sqlQuery = sqlQuery + " and datum_beginn>='" + req.body.date_begin + "'";
            }
            if(req.body.date_end!=""){
                sqlQuery = sqlQuery + " and datum_beginn<='" + req.body.date_end + "'";
            }  
            sqlQuery = sqlQuery + ";";

            //Gremientätigkeit im Ausland
            sqlQuery = sqlQuery + "select count(*) Gremientaetigkeit_ausland from Gremientaetigkeit left join Professoren on Gremientaetigkeit.person_id=Professoren.person_id left join Person on Professoren.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id where Gremientaetigkeit.land!='Deutschland'";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.body.org_name;
            if(req.body.date_begin!=""){
                sqlQuery = sqlQuery + " and datum_beginn>='" + req.body.date_begin + "'";
            }
            if(req.body.date_end!=""){
                sqlQuery = sqlQuery + " and datum_beginn<='" + req.body.date_end + "'";
            }  
            sqlQuery = sqlQuery + ";";

            //entsendete Gastwissenschaftler
            sqlQuery = sqlQuery + "select count(*) entsendete_Gastwissenschaftler from gastwissenschaftlertaetigkeit left join Mitarbeiter on gastwissenschaftlertaetigkeit.person_id_ma=Mitarbeiter.person_id left join Professoren on gastwissenschaftlertaetigkeit.person_id_prof=Professoren.person_id left join Person on Mitarbeiter.person_id=Person.person_id or Professoren.person_id=Person.person_id  left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id";
            sqlQuery = sqlQuery + " where organisationseinheit.org_id=" + req.body.org_name;
            if(req.body.date_begin!=""){
                sqlQuery = sqlQuery + " and datum_beginn>='" + req.body.date_begin + "'";
            }
            if(req.body.date_end!=""){
                sqlQuery = sqlQuery + " and datum_beginn<='" + req.body.date_end + "'";
            }  
            sqlQuery = sqlQuery + ";";

            //Wissenschaftliche Veranstaltung Fachtagung hochschulintern
            sqlQuery = sqlQuery + "select count(*) hochschulinterne_Fachtagungen from Wissenschaftliche_Veranstaltung left join beteiligt_an on Wissenschaftliche_Veranstaltung.wissen_v_id=beteiligt_an.wissen_v_id left join Organisationseinheit on beteiligt_an.org_id=Organisationseinheit.org_id where beteiligt_an.org_id is not null and Wissenschaftliche_Veranstaltung.art_der_veranstaltung='Fachtagung'";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.body.org_name;
            if(req.body.date_begin!=""){
                sqlQuery = sqlQuery + " and datum_beginn>='" + req.body.date_begin + "'";
            }
            if(req.body.date_end!=""){
                sqlQuery = sqlQuery + " and datum_beginn<='" + req.body.date_end + "'";
            }  
            sqlQuery = sqlQuery + ";";

            //Wissenschafltiche Veranstaltung Fachmesse hochschulintern
            sqlQuery = sqlQuery + "select count(*) hochschulinterne_Fachmessen from Wissenschaftliche_Veranstaltung left join beteiligt_an on Wissenschaftliche_Veranstaltung.wissen_v_id=beteiligt_an.wissen_v_id left join Organisationseinheit on beteiligt_an.org_id=Organisationseinheit.org_id where beteiligt_an.org_id is not null and Wissenschaftliche_Veranstaltung.art_der_veranstaltung='Fachmesse'";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.body.org_name;
            if(req.body.date_begin!=""){
                sqlQuery = sqlQuery + " and datum_beginn>='" + req.body.date_begin + "'";
            }
            if(req.body.date_end!=""){
                sqlQuery = sqlQuery + " and datum_beginn<='" + req.body.date_end + "'";
            }  
            sqlQuery = sqlQuery + ";";
            //Wissenschafltiche Veranstaltung sonstige Veranstaltung hochschulintern
            sqlQuery = sqlQuery + "select count(*) hochschulinterne_sonstige_Veranstaltungen from Wissenschaftliche_Veranstaltung left join beteiligt_an on Wissenschaftliche_Veranstaltung.wissen_v_id=beteiligt_an.wissen_v_id left join Organisationseinheit on beteiligt_an.org_id=Organisationseinheit.org_id where beteiligt_an.org_id is not null and Wissenschaftliche_Veranstaltung.art_der_veranstaltung='sonstige Veranstaltung'";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.body.org_name;
            if(req.body.date_begin!=""){
                sqlQuery = sqlQuery + " and datum_beginn>='" + req.body.date_begin + "'";
            }
            if(req.body.date_end!=""){
                sqlQuery = sqlQuery + " and datum_beginn<='" + req.body.date_end + "'";
            }  
            sqlQuery = sqlQuery + ";";

            //abfrage Name Organisationseinheit
            sqlQuery = sqlQuery + "select name_der_organisationseinheit from organisationseinheit where org_id=" + req.body.org_name + ";";

            //aufgenommene Gastwissenschaftler
            sqlQuery = sqlQuery + "select count(*) aufgenommene_gastwissenschaftler from aufgenommene_gastwissenschaftler left join Professoren on aufgenommene_gastwissenschaftler.person_id=Professoren.person_id left join Person on Professoren.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id"
            sqlQuery = sqlQuery + " where organisationseinheit.org_id=" + req.body.org_name;
            if (req.body.date_begin!="") {
                    sqlQuery = sqlQuery + " and datum_beginn>='" + req.body.date_begin + "'";
            }
            if (req.body.date_end!="") {
                sqlQuery = sqlQuery + " and datum_beginn<='" + req.body.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            req.session.organisationseinheit_id = req.body.org_name;
            req.session.date_begin = req.body.date_begin;
            req.session.date_end = req.body.date_end;

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {

                    var h, str, row, meta;

                    req.session.organisationseinheit_name = data.recordsets[25][0].name_der_organisationseinheit;

                    h = '<style>table,p,img{margin:30px;} body{font-family: Arial;} h1{margin-left:10;} p,td{font-size:25;}button{margin:30px;font-size: 20px;border: 3px solid black;width: 250px;height: 50px;}</style><img height="100px" width="700px" src="http://localhost:5000/images/HTWK_Zusatz_de_H_Black.jpg" alt="HTWK_Logo_Black"><h1 style="background:black;color:whitesmoke;border:20px solid black;">Forschungsdatenbank HTWK Leipzig</h1>';
                    meta = '<p>Organisationseinheit: ' + data.recordsets[25][0].name_der_organisationseinheit + '</p>' ;
                    meta = meta + '<p>Zeitraum: ' + req.body.date_begin + ' - ' + req.body.date_end + '</p>';
                    str = '<table style="border:1.5px solid black;padding:10px;"><tbody>';
                    row = '';
                    
                    //Promotionsverfahren
                    if (parseInt(parseInt(data.recordsets[0][0].abgeschlossene_Promotionsverfahren) + parseInt(data.recordsets[1][0].laufende_Promotionsverfahren))>0) {
                        row = row + '<tr><td><b>Promotionsverfahren: </b></td><td><b><a href="http://localhost:5000/show_table_promotionsverfahren">' + parseInt(parseInt(data.recordsets[0][0].abgeschlossene_Promotionsverfahren) + parseInt(data.recordsets[1][0].laufende_Promotionsverfahren)) + '</a></b></td></tr>';
                    } else {
                        row = row + '<tr><td><b>Promotionsverfahren: </b></td><td><b>' + parseInt(parseInt(data.recordsets[0][0].abgeschlossene_Promotionsverfahren) + parseInt(data.recordsets[1][0].laufende_Promotionsverfahren)) + '</b></td></tr>';
                    }

                    if (data.recordsets[0][0].abgeschlossene_Promotionsverfahren>0) {
                        row = row + '<tr><td style="text-indent:30px;">abgeschlossene Promotionsverfahren: </td><td><a href="http://localhost:5000/show_table_promotionsverfahren_a">' + data.recordsets[0][0].abgeschlossene_Promotionsverfahren + '</a></td></tr>';
                    } else {
                        row = row + '<tr><td style="text-indent:30px;">abgeschlossene Promotionsverfahren: </td><td>' + data.recordsets[0][0].abgeschlossene_Promotionsverfahren + '</td></tr>';
                    }

                    if (data.recordsets[1][0].laufende_Promotionsverfahren>0) {
                        row = row + '<tr><td style="text-indent:30px;">laufende Promotionsverfahren: </td><td><a href="http://localhost:5000/show_table_promotionsverfahren_l">' + data.recordsets[1][0].laufende_Promotionsverfahren + '</a></td></tr>';
                    } else {
                        row = row + '<tr><td style="text-indent:30px;">laufende Promotionsverfahren: </td><td>' + data.recordsets[1][0].laufende_Promotionsverfahren + '</td></tr>';
                    }
                    
                    //Graduierungsarbeiten
                    if (parseInt(parseInt(parseInt(data.recordsets[2][0].Bachelorarbeiten_ohne_Kooperationspartner) + parseInt(data.recordsets[3][0].Bachelorarbeiten_mit_Kooperationspartner)) + parseInt(parseInt(data.recordsets[4][0].Masterarbeiten_ohne_Kooperationspartner) + parseInt(data.recordsets[5][0].Masterarbeiten_mit_Kooperationspartner)))>0) {
                        row = row + '<tr style="height:20px;"></tr><tr><td><b>Graduierungsarbeiten: </b></td><td><b><a href="http://localhost:5000/show_table_graduierungsarbeiten">' + parseInt(parseInt(parseInt(data.recordsets[2][0].Bachelorarbeiten_ohne_Kooperationspartner) + parseInt(data.recordsets[3][0].Bachelorarbeiten_mit_Kooperationspartner)) + parseInt(parseInt(data.recordsets[4][0].Masterarbeiten_ohne_Kooperationspartner) + parseInt(data.recordsets[5][0].Masterarbeiten_mit_Kooperationspartner))) + '</a></b></td></tr>';
                    } else {
                        row = row + '<tr style="height:20px;"></tr><tr><td><b>Graduierungsarbeiten: </b></td><td><b>' + parseInt(parseInt(parseInt(data.recordsets[2][0].Bachelorarbeiten_ohne_Kooperationspartner) + parseInt(data.recordsets[3][0].Bachelorarbeiten_mit_Kooperationspartner)) + parseInt(parseInt(data.recordsets[4][0].Masterarbeiten_ohne_Kooperationspartner) + parseInt(data.recordsets[5][0].Masterarbeiten_mit_Kooperationspartner))) + '</b></td></tr>';
                    }

                    if (parseInt(parseInt(data.recordsets[2][0].Bachelorarbeiten_ohne_Kooperationspartner) + parseInt(data.recordsets[3][0].Bachelorarbeiten_mit_Kooperationspartner))>0) {
                        row = row + '<tr><td style="text-indent:30px;"><b> davon Bachelorarbeiten: </b></td><td><b><a href="http://localhost:5000/show_table_graduierungsarbeiten_ba">' + parseInt(parseInt(data.recordsets[2][0].Bachelorarbeiten_ohne_Kooperationspartner) + parseInt(data.recordsets[3][0].Bachelorarbeiten_mit_Kooperationspartner)) + '</a></b></td></tr>';
                    } else {
                        row = row + '<tr><td style="text-indent:30px;"><b> davon Bachelorarbeiten: </b></td><td><b>' + parseInt(parseInt(data.recordsets[2][0].Bachelorarbeiten_ohne_Kooperationspartner) + parseInt(data.recordsets[3][0].Bachelorarbeiten_mit_Kooperationspartner)) + '</b></td></tr>';
                    }

                    if (data.recordsets[2][0].Bachelorarbeiten_ohne_Kooperationspartner>0) {
                        row = row + '<tr><td style="text-indent:60px;">ohne Kooperationspartner: </td><td><a href="http://localhost:5000/show_table_graduierungsarbeiten_ba_o_k">' + data.recordsets[2][0].Bachelorarbeiten_ohne_Kooperationspartner + '</a></td></tr>';
                    } else {
                        row = row + '<tr><td style="text-indent:60px;">ohne Kooperationspartner: </td><td>' + data.recordsets[2][0].Bachelorarbeiten_ohne_Kooperationspartner + '</td></tr>';
                    }   

                    if (data.recordsets[3][0].Bachelorarbeiten_mit_Kooperationspartner>0) {
                        row = row + '<tr><td style="text-indent:60px;">mit Kooperationspartner: </td><td><a href="http://localhost:5000/show_table_graduierungsarbeiten_ba_m_k">' + data.recordsets[3][0].Bachelorarbeiten_mit_Kooperationspartner + '</a></td></tr>';
                    } else {
                        row = row + '<tr><td style="text-indent:60px;">mit Kooperationspartner: </td><td>' + data.recordsets[3][0].Bachelorarbeiten_mit_Kooperationspartner + '</td></tr>';
                    }

                    if (parseInt(parseInt(data.recordsets[4][0].Masterarbeiten_ohne_Kooperationspartner) + parseInt(data.recordsets[5][0].Masterarbeiten_mit_Kooperationspartner))>0) {
                        row = row + '<tr><td style="text-indent:30px;"><b> davon Masterarbeiten: </b></td><td><b><a href="http://localhost:5000/show_table_graduierungsarbeiten_ma">' + parseInt(parseInt(data.recordsets[4][0].Masterarbeiten_ohne_Kooperationspartner) + parseInt(data.recordsets[5][0].Masterarbeiten_mit_Kooperationspartner)) + '</a></b></td></tr>';

                    } else {
                        row = row + '<tr><td style="text-indent:30px;"><b> davon Masterarbeiten: </b></td><td><b>' + parseInt(parseInt(data.recordsets[4][0].Masterarbeiten_ohne_Kooperationspartner) + parseInt(data.recordsets[5][0].Masterarbeiten_mit_Kooperationspartner)) + '</b></td></tr>';

                    }

                    if (data.recordsets[4][0].Masterarbeiten_ohne_Kooperationspartner>0) {
                        row = row + '<tr><td style="text-indent:60px;">ohne Kooperationspartner: </td><td><a href="http://localhost:5000/show_table_graduierungsarbeiten_ma_o_k">' + data.recordsets[4][0].Masterarbeiten_ohne_Kooperationspartner + '</a></td></tr>';
                    } else {
                        row = row + '<tr><td style="text-indent:60px;">ohne Kooperationspartner: </td><td>' + data.recordsets[4][0].Masterarbeiten_ohne_Kooperationspartner + '</td></tr>';
                    }

                    if (data.recordsets[5][0].Masterarbeiten_mit_Kooperationspartner>0) {
                        row = row + '<tr><td style="text-indent:60px;">mit Kooperationspartner: </td><td><a href="http://localhost:5000/show_table_graduierungsarbeiten_ma_m_k">' + data.recordsets[5][0].Masterarbeiten_mit_Kooperationspartner + '</a></td></tr>';
                    } else {
                        row = row + '<tr><td style="text-indent:60px;">mit Kooperationspartner: </td><td>' + data.recordsets[5][0].Masterarbeiten_mit_Kooperationspartner + '</td></tr>';
                    }
                    
                    //Wissenschaftliche Veroeffentlichungen
                    if (parseInt(parseInt(data.recordsets[6][0].Wissen_Veroeffentlichungen_M_S) + parseInt(data.recordsets[7][0].Fachbeitraege_in_wissen_Zeitschriften) + parseInt(data.recordsets[8][0].Kurzbeitraege) + parseInt(data.recordsets[9][0].Ver_im_Rahmen_v_Veranst) + parseInt(data.recordsets[10][0].sonstige_Veroeffentlichungen))>0) {
                        row = row + '<tr style="height:20px;"></tr><tr><td><b>Wissenschaftl. Veröffentlichungen: </b></td><td><b><a href="http://localhost:5000/show_table_wissen_veroeffentlichungen">' + parseInt(parseInt(data.recordsets[6][0].Wissen_Veroeffentlichungen_M_S) + parseInt(data.recordsets[7][0].Fachbeitraege_in_wissen_Zeitschriften) + parseInt(data.recordsets[8][0].Kurzbeitraege) + parseInt(data.recordsets[9][0].Ver_im_Rahmen_v_Veranst) + parseInt(data.recordsets[10][0].sonstige_Veroeffentlichungen)) + '</a></b></td></tr>';
                    } else {
                        row = row + '<tr style="height:20px;"></tr><tr><td><b>Wissenschaftl. Veröffentlichungen: </b></td><td><b>' + parseInt(parseInt(data.recordsets[6][0].Wissen_Veroeffentlichungen_M_S) + parseInt(data.recordsets[7][0].Fachbeitraege_in_wissen_Zeitschriften) + parseInt(data.recordsets[8][0].Kurzbeitraege) + parseInt(data.recordsets[9][0].Ver_im_Rahmen_v_Veranst) + parseInt(data.recordsets[10][0].sonstige_Veroeffentlichungen)) + '</b></td></tr>';
                    }

                    if (data.recordsets[6][0].Wissen_Veroeffentlichungen_M_S>0) {
                        row = row + '<tr><td style="text-indent:30px;">Monografien/Beiträge in Sammelbänden: </td><td><a href="http://localhost:5000/show_table_wissen_veroeffentlichungen_m_s">' + data.recordsets[6][0].Wissen_Veroeffentlichungen_M_S + '</a></td></tr>';
                    } else {
                        row = row + '<tr><td style="text-indent:30px;">Monografien/Beiträge in Sammelbänden: </td><td>' + data.recordsets[6][0].Wissen_Veroeffentlichungen_M_S + '</td></tr>';
                    }

                    if (data.recordsets[7][0].Fachbeitraege_in_wissen_Zeitschriften>0) {
                        row = row + '<tr><td style="text-indent:30px;">Fachbeiträge in wissenschaftl. Zeitschriften: </td><td><a href="http://localhost:5000/show_table_wissen_veroeffentlichungen_w_z">' + data.recordsets[7][0].Fachbeitraege_in_wissen_Zeitschriften + '</a></td></tr>';
                    } else {
                        row = row + '<tr><td style="text-indent:30px;">Fachbeiträge in wissenschaftl. Zeitschriften: </td><td>' + data.recordsets[7][0].Fachbeitraege_in_wissen_Zeitschriften + '</td></tr>';
                    }

                    if (data.recordsets[8][0].Kurzbeitraege>0) {
                        row = row + '<tr><td style="text-indent:30px;">Kurzbeiträge: </td><td><a href="http://localhost:5000/show_table_wissen_veroeffentlichungen_k">' + data.recordsets[8][0].Kurzbeitraege + '</a></td></tr>';
                    } else {
                        row = row + '<tr><td style="text-indent:30px;">Kurzbeiträge: </td><td>' + data.recordsets[8][0].Kurzbeitraege + '</td></tr>';
                    }

                    if (data.recordsets[9][0].Ver_im_Rahmen_v_Veranst>0) {
                        row = row + '<tr><td style="text-indent:30px;width:600px;">Veröffentlichungen im Rahmen von Veranstaltungen: </td><td><a href="http://localhost:5000/show_table_wissen_veroeffentlichungen_w_v">' + data.recordsets[9][0].Ver_im_Rahmen_v_Veranst + '</a></td></tr>';
                    } else {
                        row = row + '<tr><td style="text-indent:30px;width:600px;">Veröffentlichungen im Rahmen von Veranstaltungen: </td><td>' + data.recordsets[9][0].Ver_im_Rahmen_v_Veranst + '</td></tr>';
                    }

                    if (data.recordsets[10][0].sonstige_Veroeffentlichungen>0) {
                        row = row + '<tr><td style="text-indent:30px;">sonstige Veröffentlichungen: </td><td><a href="http://localhost:5000/show_table_wissen_veroeffentlichungen_s_v">' + data.recordsets[10][0].sonstige_Veroeffentlichungen + '</a></td></tr>';
                    } else {
                        row = row + '<tr><td style="text-indent:30px;">sonstige Veröffentlichungen: </td><td>' + data.recordsets[10][0].sonstige_Veroeffentlichungen + '</td></tr>';
                    }
                    
                    //FuE-Projekte
                    if (parseInt(parseInt(data.recordsets[11][0].FuE_Projekte_mit_Kooperationspartner) + parseInt(data.recordsets[12][0].FuE_Projekte_ohne_Kooperationspartner))>0) {
                        row = row + '<tr style="height:20px;"></tr><tr><td><b>FuE-Projekte: </b></td><td><b><a href="http://localhost:5000/show_table_fue_projekte">' + parseInt(parseInt(data.recordsets[11][0].FuE_Projekte_mit_Kooperationspartner) + parseInt(data.recordsets[12][0].FuE_Projekte_ohne_Kooperationspartner)) + '</a></b></td></tr>';
                    } else {
                        row = row + '<tr style="height:20px;"></tr><tr><td><b>FuE-Projekte: </b></td><td><b>' + parseInt(parseInt(data.recordsets[11][0].FuE_Projekte_mit_Kooperationspartner) + parseInt(data.recordsets[12][0].FuE_Projekte_ohne_Kooperationspartner)) + '</b></td></tr>';
                    }

                    if (data.recordsets[11][0].FuE_Projekte_mit_Kooperationspartner>0) {
                        row = row + '<tr><td style="text-indent:30px;">mit Kooperationspartner: </td><td><a href="http://localhost:5000/show_table_fue_projekte_m_k">' + data.recordsets[11][0].FuE_Projekte_mit_Kooperationspartner + '</a></td></tr>';
                    } else {
                        row = row + '<tr><td style="text-indent:30px;">mit Kooperationspartner: </td><td>' + data.recordsets[11][0].FuE_Projekte_mit_Kooperationspartner + '</td></tr>';
                    }

                    if (data.recordsets[12][0].FuE_Projekte_ohne_Kooperationspartner>0) {
                        row = row + '<tr><td style="text-indent:30px;">ohne Kooperationspartner: </td><td><a href="http://localhost:5000/show_table_fue_projekte_o_k">' + data.recordsets[12][0].FuE_Projekte_ohne_Kooperationspartner + '</a></td></tr>';
                    } else {
                        row = row + '<tr><td style="text-indent:30px;">ohne Kooperationspartner: </td><td>' + data.recordsets[12][0].FuE_Projekte_ohne_Kooperationspartner + '</td></tr>';
                    }
                    
                    //Vorträge
                    if (parseInt(parseInt(data.recordsets[13][0].Vortraege_auf_wissen_Veranstaltungen) + parseInt(data.recordsets[14][0].sonstige_Vortraege))>0) {
                        row = row + '<tr style="height:20px;"></tr><tr><td><b>Vorträge: </b></td><td><b><a href="http://localhost:5000/show_table_vortraege">' + parseInt(parseInt(data.recordsets[13][0].Vortraege_auf_wissen_Veranstaltungen) + parseInt(data.recordsets[14][0].sonstige_Vortraege)) + '</a></b></td></tr>';
                    } else {
                        row = row + '<tr style="height:20px;"></tr><tr><td><b>Vorträge: </b></td><td><b>' + parseInt(parseInt(data.recordsets[13][0].Vortraege_auf_wissen_Veranstaltungen) + parseInt(data.recordsets[14][0].sonstige_Vortraege)) + '</b></td></tr>';
                    }

                    if (data.recordsets[13][0].Vortraege_auf_wissen_Veranstaltungen>0) {
                        row = row + '<tr><td style="text-indent:30px;">Vorträge auf wissenschaftl. Veranstaltungen: </td><td><a href="http://localhost:5000/show_table_vortraege_v">' + data.recordsets[13][0].Vortraege_auf_wissen_Veranstaltungen + '</a></td></tr>';
                    } else {
                        row = row + '<tr><td style="text-indent:30px;">Vorträge auf wissenschaftl. Veranstaltungen: </td><td>' + data.recordsets[13][0].Vortraege_auf_wissen_Veranstaltungen + '</td></tr>';
                    }

                    if (data.recordsets[14][0].sonstige_Vortraege>0) {
                        row = row + '<tr><td style="text-indent:30px;">sonstige Vorträge: </td><td><a href="http://localhost:5000/show_table_vortraege_s">' + data.recordsets[14][0].sonstige_Vortraege + '</a></td></tr>';
                    } else {
                        row = row + '<tr><td style="text-indent:30px;">sonstige Vorträge: </td><td>' + data.recordsets[14][0].sonstige_Vortraege + '</td></tr>';
                    }
                    
                    //Existenzgründung
                    if (data.recordsets[15][0].Existenzgruendungen>0) {
                        row = row + '<tr style="height:20px;"></tr><tr><td><b>Existenzgründungen: </b></td><td><b><a href="http://localhost:5000/show_table_existenzgruendung">' + data.recordsets[15][0].Existenzgruendungen + '</a></b></td></tr>';
                    } else {
                        row = row + '<tr style="height:20px;"></tr><tr><td><b>Existenzgründungen: </b></td><td><b>' + data.recordsets[15][0].Existenzgruendungen + '</b></td></tr>';
                    }
                    
                    //Schutzrechte
                    if (data.recordsets[16][0].Anmeldung_Schutzrechte>0) {
                        row = row + '<tr style="height:20px;"></tr><tr><td><b>Anmeldung Schutzrechte: </b></td><td><b><a href="http://localhost:5000/show_table_schutzrechte">' + data.recordsets[16][0].Anmeldung_Schutzrechte + '</a></b></td></tr>';
                    } else {
                        row = row + '<tr style="height:20px;"></tr><tr><td><b>Anmeldung Schutzrechte: </b></td><td><b>' + data.recordsets[16][0].Anmeldung_Schutzrechte + '</b></td></tr>';
                    }
                    
                    //Auszeichnungen
                    if (parseInt(parseInt(data.recordsets[17][0].Auszeichnungen_inland) + parseInt(data.recordsets[18][0].Auszeichnungen_ausland))>0) {
                        row = row + '<tr style="height:20px;"></tr><tr><td><b>Auszeichnungen: </b></td><td><b><a href="http://localhost:5000/show_table_auszeichnungen">' + parseInt(parseInt(data.recordsets[17][0].Auszeichnungen_inland) + parseInt(data.recordsets[18][0].Auszeichnungen_ausland)) + '</a></b></td></tr>';
                    } else {
                        row = row + '<tr style="height:20px;"></tr><tr><td><b>Auszeichnungen: </b></td><td><b>' + parseInt(parseInt(data.recordsets[17][0].Auszeichnungen_inland) + parseInt(data.recordsets[18][0].Auszeichnungen_ausland)) + '</b></td></tr>';
                    }

                    if (data.recordsets[17][0].Auszeichnungen_inland>0) {
                        row = row + '<tr><td style="text-indent:30px;">Auszeichnungen (Inland): </td><td><a href="http://localhost:5000/show_table_auszeichnungen_i">' + data.recordsets[17][0].Auszeichnungen_inland + '</a></td></tr>';
                    } else {
                        row = row + '<tr><td style="text-indent:30px;">Auszeichnungen (Inland): </td><td>' + data.recordsets[17][0].Auszeichnungen_inland + '</td></tr>';
                    }

                    if (data.recordsets[18][0].Auszeichnungen_ausland>0) {
                        row = row + '<tr><td style="text-indent:30px;">Auszeichnungen (Ausland): </td><td><a href="http://localhost:5000/show_table_auszeichnungen_a">' + data.recordsets[18][0].Auszeichnungen_ausland + '</a></td></tr>';
                    } else {
                        row = row + '<tr><td style="text-indent:30px;">Auszeichnungen (Ausland): </td><td>' + data.recordsets[18][0].Auszeichnungen_ausland + '</td></tr>';
                    }   
                    
                    //Gremientätigkeit
                    if (parseInt(parseInt(data.recordsets[19][0].Gremientaetigkeit_inland) + parseInt(data.recordsets[20][0].Gremientaetigkeit_ausland))>0) {
                        row = row + '<tr style="height:20px;"></tr><tr><td><b>Gremientätigkeit: </b></td><td><b><a href="http://localhost:5000/show_table_gremientaetigkeit">' + parseInt(parseInt(data.recordsets[19][0].Gremientaetigkeit_inland) + parseInt(data.recordsets[20][0].Gremientaetigkeit_ausland)) + '</a></b></td></tr>';
                    } else {
                        row = row + '<tr style="height:20px;"></tr><tr><td><b>Gremientätigkeit: </b></td><td><b>' + parseInt(parseInt(data.recordsets[19][0].Gremientaetigkeit_inland) + parseInt(data.recordsets[20][0].Gremientaetigkeit_ausland)) + '</b></td></tr>';
                    }

                    if (data.recordsets[19][0].Gremientaetigkeit_inland>0) {
                        row = row + '<tr><td style="text-indent:30px;">Gremientätigkeit (Inland): </td><td><a href="http://localhost:5000/show_table_gremientaetigkeit_i">' + data.recordsets[19][0].Gremientaetigkeit_inland + '</a></td></tr>';
                    } else {
                        row = row + '<tr><td style="text-indent:30px;">Gremientätigkeit (Inland): </td><td>' + data.recordsets[19][0].Gremientaetigkeit_inland + '</td></tr>';
                    }

                    if (data.recordsets[20][0].Gremientaetigkeit_ausland>0) {
                        row = row + '<tr><td style="text-indent:30px;">Gremientätigkeit (Ausland): </td><td><a href="http://localhost:5000/show_table_gremientaetigkeit_a">' + data.recordsets[20][0].Gremientaetigkeit_ausland + '</a></td></tr>';
                    } else {
                        row = row + '<tr><td style="text-indent:30px;">Gremientätigkeit (Ausland): </td><td>' + data.recordsets[20][0].Gremientaetigkeit_ausland + '</td></tr>';
                    }
                    
                    //Gastwissenschaftler
                    row = row + '<tr style="height:20px;"></tr><tr><td><b>Gastwissenschaftler: </b></td><td><b>' + parseInt(parseInt(data.recordsets[21][0].entsendete_Gastwissenschaftler) + parseInt(data.recordsets[26][0].aufgenommene_gastwissenschaftler)) + '</b></td></tr>';
                    

                    if (data.recordsets[21][0].entsendete_Gastwissenschaftler>0) {
                        row = row + '<tr></tr><tr><td style="text-indent:30px;">entsendete Gastwissenschaftler: </td><td><a href="http://localhost:5000/show_table_gastwissenschaftler_e">' + data.recordsets[21][0].entsendete_Gastwissenschaftler + '</a></td></tr>';
                    } else {
                        row = row + '<tr></tr><tr><td style="text-indent:30px;">entsendete Gastwissenschaftler: </td><td>' + data.recordsets[21][0].entsendete_Gastwissenschaftler + '</td></tr>';
                    }

                    if (data.recordsets[26][0].aufgenommene_gastwissenschaftler>0) {
                        row = row + '<tr></tr><tr><td style="text-indent:30px;">aufgenommene Gastwissenschaftler: </td><td><a href="http://localhost:5000/show_table_gastwissenschaftler_a">' + data.recordsets[26][0].aufgenommene_gastwissenschaftler + '</a></td></tr>';
                    } else {
                        row = row + '<tr></tr><tr><td style="text-indent:30px;">aufgenommene Gastwissenschaftler: </td><td><b>' + data.recordsets[26][0].aufgenommene_gastwissenschaftler + '</td></tr>';
                    }

                    //Wissenschaftliche Veranstaltungen
                    if (parseInt(parseInt(data.recordsets[22][0].hochschulinterne_Fachtagungen) + parseInt(data.recordsets[23][0].hochschulinterne_Fachmessen) + parseInt(data.recordsets[24][0].hochschulinterne_sonstige_Veranstaltungen))>0) {
                        row = row + '<tr style="height:20px;"></tr><tr><td><b>Wissenschaftliche Veranstaltungen (hochschulintern): </b></td><td><b><a href="http://localhost:5000/show_table_wissen_veranstaltung">' + parseInt(parseInt(data.recordsets[22][0].hochschulinterne_Fachtagungen) + parseInt(data.recordsets[23][0].hochschulinterne_Fachmessen) + parseInt(data.recordsets[24][0].hochschulinterne_sonstige_Veranstaltungen)) + '</a></b></td></tr>';
                    } else {
                        row = row + '<tr style="height:20px;"></tr><tr><td><b>Wissenschaftliche Veranstaltungen (hochschulintern): </b></td><td><b>' + parseInt(parseInt(data.recordsets[22][0].hochschulinterne_Fachtagungen) + parseInt(data.recordsets[23][0].hochschulinterne_Fachmessen) + parseInt(data.recordsets[24][0].hochschulinterne_sonstige_Veranstaltungen)) + '</b></td></tr>';
                    }

                    if (data.recordsets[22][0].hochschulinterne_Fachtagungen>0) {
                        row = row + '<tr><td style="text-indent:30px;">davon Fachtagungen: </td><td><a href="http://localhost:5000/show_table_wissen_veranstaltung_ft">' + data.recordsets[22][0].hochschulinterne_Fachtagungen + '</a></td></tr>';
                    } else {
                        row = row + '<tr><td style="text-indent:30px;">davon Fachtagungen: </td><td>' + data.recordsets[22][0].hochschulinterne_Fachtagungen + '</td></tr>';
                    }
                    if (data.recordsets[23][0].hochschulinterne_Fachmessen>0) {
                        row = row + '<tr><td style="text-indent:30px;">davon Fachmessen: </td><td><a href="http://localhost:5000/show_table_wissen_veranstaltung_fm">' + data.recordsets[23][0].hochschulinterne_Fachmessen + '</a></td></tr>';
                    } else {
                        row = row + '<tr><td style="text-indent:30px;">davon Fachmessen: </td><td>' + data.recordsets[23][0].hochschulinterne_Fachmessen + '</td></tr>';
                    }
                    if (data.recordsets[24][0].hochschulinterne_sonstige_Veranstaltungen>0) {
                        row = row + '<tr><td style="text-indent:30px;">davon sonstige Veranstaltungen: </td><td><a href="http://localhost:5000/show_table_wissen_veranstaltung_sv">' + data.recordsets[24][0].hochschulinterne_sonstige_Veranstaltungen + '</a></td></tr>';
                    } else {
                        row = row + '<tr><td style="text-indent:30px;">davon sonstige Veranstaltungen: </td><td>' + data.recordsets[24][0].hochschulinterne_sonstige_Veranstaltungen + '</td></tr>';
                    }

                    str = str + row + '</tbody></table></br><form action="/" method="get"><button>Zurück</button></form>';

                    console.table(data.recordsets);
                    console.log(data.recordsets[1][0].laufende_Promotionsverfahren);
                    res.send(h+meta+str);
                }

                
                sql.close; 
            });
        }
    })
});

//Folgende Funktionen sind für die Darstellung der jeweiligen Tabellen da

app.get('/show_table_schutzrechte',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select s_id,titel,status,datum_der_anmeldung,kennzeichen_veroeffentlichungsnummer,name,vorname from Schutzrechte left join Person on Schutzrechte.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id";
            sqlQuery = sqlQuery + " where organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and datum_der_anmeldung>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and datum_der_anmeldung<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Schutzrechte',req,data,['S_id','Titel','Status','Datum der Anmeldung','Kennzeichen/Veröffentlichungsnummer','Name','Vorname'],['s_id','titel','status','datum_der_anmeldung','kennzeichen_veroeffentlichungsnummer','name','vorname']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_existenzgruendung',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select existenzgruendung.ex_id,name_der_gruendung,ort,gruendungsdatum,rechtsform,status_der_gruendung,name,vorname from existenzgruendung left join gruendet_aus on Existenzgruendung.ex_id=gruendet_aus.ex_id left join Person on gruendet_aus.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id";
            sqlQuery = sqlQuery + " where organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and gruendungsdatum>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and gruendungsdatum<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Existenzgründung',req,data,['ex_id','Name der Gründung','Ort','Gründungsdatum','Rechtsform','Status der Gründung','Name','Vorname'],['ex_id','name_der_gruendung','ort','gruendungsdatum','rechtsform','status_der_gruendung','name','vorname']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_auszeichnungen',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select af_id,titel_der_auszeichnung,art_des_preises,datum,erlaeuterung,ort,land,auszeichnung_ist_dotiert,vergeben_durch,name,vorname from auszeichnung left join Person on auszeichnung.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id";
            sqlQuery = sqlQuery + " where organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and datum>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and datum<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Auszeichnungen',req,data,['Af_id','Titel der Auszeichnung','Art des Preises','Datum','Erläuterung','Ort','Land','Auszeichnung ist dotier?','Vergeben durch','Name','Vorname'],['af_id','titel_der_auszeichnung','art_des_preises','datum','erlaeuterung','ort','land','auszeichnung_ist_dotiert','vergeben_durch','name','vorname']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_auszeichnungen_i',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select af_id,titel_der_auszeichnung,art_des_preises,datum,erlaeuterung,ort,land,auszeichnung_ist_dotiert,vergeben_durch,name,vorname from auszeichnung left join Person on auszeichnung.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id where auszeichnung.land='Deutschland'";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and datum>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and datum<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Auszeichnungen (Inland)',req,data,['Af_id','Titel der Auszeichnung','Art des Preises','Datum','Erläuterung','Ort','Land','Auszeichnung ist dotier?','Vergeben durch','Name','Vorname'],['af_id','titel_der_auszeichnung','art_des_preises','datum','erlaeuterung','ort','land','auszeichnung_ist_dotiert','vergeben_durch','name','vorname']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_auszeichnungen_a',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select af_id,titel_der_auszeichnung,art_des_preises,datum,erlaeuterung,ort,land,auszeichnung_ist_dotiert,vergeben_durch,name,vorname from auszeichnung left join Person on auszeichnung.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id where auszeichnung.land!='Deutschland'";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and datum>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and datum<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Auszeichnungen (Ausland)',req,data,['Af_id','Titel der Auszeichnung','Art des Preises','Datum','Erläuterung','Ort','Land','Auszeichnung ist dotier?','Vergeben durch','Name','Vorname'],['af_id','titel_der_auszeichnung','art_des_preises','datum','erlaeuterung','ort','land','auszeichnung_ist_dotiert','vergeben_durch','name','vorname']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_gremientaetigkeit',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select mwgg_id,beschreibung_der_taetigkeit,gremium_einrichtung,datum_beginn,funktion,erlaeuterung,ort,land,name,vorname from Gremientaetigkeit left join Professoren on Gremientaetigkeit.person_id=Professoren.person_id left join Person on Professoren.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id";
            sqlQuery = sqlQuery + " where organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and datum_beginn>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and datum_beginn<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Gremientätikeit',req,data,['mwgg_id','Beschreibung der Tätigkeit','Gremium/Einrichtung','Datum beginn','Funktion','Erläuterung','Ort','Land','Name','Vorname'],['mwgg_id','beschreibung_der_taetigkeit','gremium_einrichtung','datum_beginn','funktion','erlaeuterung','ort','land','name','vorname']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_gremientaetigkeit_i',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select mwgg_id,beschreibung_der_taetigkeit,gremium_einrichtung,datum_beginn,funktion,erlaeuterung,ort,land,name,vorname from Gremientaetigkeit left join Professoren on Gremientaetigkeit.person_id=Professoren.person_id left join Person on Professoren.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id where Gremientaetigkeit.land='Deutschland'";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and datum_beginn>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and datum_beginn<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Gremientätikeit',req,data,['mwgg_id','Beschreibung der Tätigkeit','Gremium/Einrichtung','Datum beginn','Funktion','Erläuterung','Ort','Land','Name','Vorname'],['mwgg_id','beschreibung_der_taetigkeit','gremium_einrichtung','datum_beginn','funktion','erlaeuterung','ort','land','name','vorname']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_gremientaetigkeit_a',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select mwgg_id,beschreibung_der_taetigkeit,gremium_einrichtung,datum_beginn,funktion,erlaeuterung,ort,land,name,vorname from Gremientaetigkeit left join Professoren on Gremientaetigkeit.person_id=Professoren.person_id left join Person on Professoren.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id where Gremientaetigkeit.land!='Deutschland'";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and datum_beginn>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and datum_beginn<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Gremientätikeit',req,data,['mwgg_id','Beschreibung der Tätigkeit','Gremium/Einrichtung','Datum beginn','Funktion','Erläuterung','Ort','Land','Name','Vorname'],['mwgg_id','beschreibung_der_taetigkeit','gremium_einrichtung','datum_beginn','funktion','erlaeuterung','ort','land','name','vorname']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_gastwissenschaftler_e',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select gw_id,arbeitsthema_anlass,foerderprogramm,datum_beginn,datum_ende,anmerkung,aufnehmende_einrichtung,name,vorname from gastwissenschaftlertaetigkeit left join Mitarbeiter on gastwissenschaftlertaetigkeit.person_id_ma=Mitarbeiter.person_id left join Professoren on gastwissenschaftlertaetigkeit.person_id_prof=Professoren.person_id left join Person on Mitarbeiter.person_id=Person.person_id or Professoren.person_id=Person.person_id  left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id";
            sqlQuery = sqlQuery + " where organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and datum_beginn>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and datum_beginn<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Gastwissenschaftlertätigkeiten (entsendet)',req,data,['gw_id','Arbeitsthema/Anlass','Förderprogramm','Datum Beginn','Datum Ende','Anmerkung','aufnehmende Einrichtung','Name','Vorname'],['gw_id','arbeitsthema_anlass','foerderprogramm','datum_beginn','datum_ende','anmerkung','aufnehmende_einrichtung','name','vorname']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_gastwissenschaftler_a',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select aufgenommene_gastwissenschaftler.agw_id,aufgenommene_gastwissenschaftler.name gname,aufgenommene_gastwissenschaftler.vorname gvorname,arbeitsthema,datum_beginn,datum_ende,entsendende_einrichtung,person.name bname,person.vorname bvorname from aufgenommene_gastwissenschaftler left join Professoren on aufgenommene_gastwissenschaftler.person_id=Professoren.person_id left join Person on Professoren.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id";
            sqlQuery = sqlQuery + " where organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and datum_beginn>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and datum_beginn<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Gastwissenschaftlertätigkeiten (aufgenommen)',req,data,['agw_id','Name','Vorname','Arbeitsthema','Datum Beginn','Datum Ende','entsendende Einrichtung','Name (Betreuer)','Vorname (Betreuer)'],['agw_id','gname','gvorname','arbeitsthema','datum_beginn','datum_ende','entsendende_einrichtung','bname','bvorname']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_vortraege',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select wissenschaftliche_vortraege.vortr_id,titel,datum,ort,land,erlaeuterung,name,vorname from wissenschaftliche_vortraege left join haelt on wissenschaftliche_vortraege.vortr_id=haelt.vortr_id left join Person on haelt.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id";
            sqlQuery = sqlQuery + " where organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and datum>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and datum<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Vorträge',req,data,['vortr_id','Titel','Datum','Ort','Land','Erläuterung','Name','Vorname'],['vortr_id','titel','datum','ort','land','erlaeuterung','name','vorname']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_vortraege_v',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select wissenschaftliche_vortraege.vortr_id,titel,datum,ort,land,erlaeuterung,name,vorname from wissenschaftliche_vortraege left join haelt on wissenschaftliche_vortraege.vortr_id=haelt.vortr_id left join Person on haelt.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id left join gehalten_im_Rahmen_von on wissenschaftliche_vortraege.vortr_id=gehalten_im_Rahmen_von.vortr_id where gehalten_im_Rahmen_von.vortr_id is not null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and datum>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and datum<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Vorträge (auf wissenschaftl. Veranstaltungen',req,data,['vortr_id','Titel','Datum','Ort','Land','Erläuterung','Name','Vorname'],['vortr_id','titel','datum','ort','land','erlaeuterung','name','vorname']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_vortraege_s',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select wissenschaftliche_vortraege.vortr_id,titel,datum,ort,land,erlaeuterung,name,vorname from wissenschaftliche_vortraege left join haelt on wissenschaftliche_vortraege.vortr_id=haelt.vortr_id left join Person on haelt.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id left join gehalten_im_Rahmen_von on wissenschaftliche_vortraege.vortr_id=gehalten_im_Rahmen_von.vortr_id where gehalten_im_Rahmen_von.vortr_id is null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and datum>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and datum<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Vorträge (sonstige)',req,data,['vortr_id','Titel','Datum','Ort','Land','Erläuterung','Name','Vorname'],['vortr_id','titel','datum','ort','land','erlaeuterung','name','vorname']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_fue_projekte',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select fue_id,projekttitel_deutsch,projekttitel_englisch,projektart,beginn,datum_abschluss,kurzbeschreibung_deutsch,kurzbeschreibung_englisch,name,vorname from fue_projekte left join Professoren on fue_projekte.person_id=Professoren.person_id left join Person on Professoren.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id";
            sqlQuery = sqlQuery + " where organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and beginn>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and beginn<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('FuE-Projekte',req,data,['fue_id','Projekttitel (deutsch)','Projekttitel (englisch)','Projektart','Beginn','(geplanter) Abschluss','Kurzbeschreibung (deutsch)','Kurzbeschreibung (englisch)','Name (Leiter)','vorname (Leiter)'],['fue_id','projekttitel_deutsch','projekttitel_englisch','projektart','beginn','datum_abschluss','kurzbeschreibung_deutsch','kurzbeschreibung_englisch','name','vorname']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_fue_projekte_m_k',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select fue_projekte.fue_id,projekttitel_deutsch,projekttitel_englisch,projektart,beginn,datum_abschluss,kurzbeschreibung_deutsch,kurzbeschreibung_englisch,name,vorname from fue_projekte left join Professoren on fue_projekte.person_id=Professoren.person_id left join Person on Professoren.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id left join ist_koop_von_proj on fue_projekte.fue_id=ist_koop_von_proj.fue_id where ist_koop_von_proj.fue_id is not null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and beginn>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and beginn<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('FuE-Projekte (mit Kooperationspartner)',req,data,['fue_id','Projekttitel (deutsch)','Projekttitel (englisch)','Projektart','Beginn','(geplanter) Abschluss','Kurzbeschreibung (deutsch)','Kurzbeschreibung (englisch)','Name (Leiter)','vorname (Leiter)'],['fue_id','projekttitel_deutsch','projekttitel_englisch','projektart','beginn','datum_abschluss','kurzbeschreibung_deutsch','kurzbeschreibung_englisch','name','vorname']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_fue_projekte_o_k',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select fue_projekte.fue_id,projekttitel_deutsch,projekttitel_englisch,projektart,beginn,datum_abschluss,kurzbeschreibung_deutsch,kurzbeschreibung_englisch,name,vorname from fue_projekte left join Professoren on fue_projekte.person_id=Professoren.person_id left join Person on Professoren.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id left join ist_koop_von_proj on fue_projekte.fue_id=ist_koop_von_proj.fue_id where ist_koop_von_proj.fue_id is null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and beginn>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and beginn<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('FuE-Projekte (ohne Kooperationspartner)',req,data,['fue_id','Projekttitel (deutsch)','Projekttitel (englisch)','Projektart','Beginn','(geplanter) Abschluss','Kurzbeschreibung (deutsch)','Kurzbeschreibung (englisch)','Name (Leiter)','vorname (Leiter)'],['fue_id','projekttitel_deutsch','projekttitel_englisch','projektart','beginn','datum_abschluss','kurzbeschreibung_deutsch','kurzbeschreibung_englisch','name','vorname']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_graduierungsarbeiten',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select graduierungsarbeit.grad_id,bachelor_master,titel_der_arbeit,datum_abschluss,note,person.name pname,vorname,kooperationspartner.name kname from graduierungsarbeit left join Studenten on graduierungsarbeit.person_id=Studenten.person_id left join Person on Studenten.person_id=person.person_id left join gehoert_zu on person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id left join ist_koop_von_ga on graduierungsarbeit.grad_id=ist_koop_von_ga.grad_id left join kooperationspartner on ist_koop_von_ga.koop_id=kooperationspartner.koop_id";
            sqlQuery = sqlQuery + " where organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and datum_abschluss>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and datum_abschluss<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Graduierungsarbeiten',req,data,['grad_id','Bachelor/Masster','Titel der Arbeit','Datum Abschluss','Note','Name','Vorname','Kooperationspartner'],['grad_id','bachelor_master','titel_der_arbeit','datum_abschluss','note','pname','vorname','kname']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_graduierungsarbeiten_ba',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select graduierungsarbeit.grad_id,bachelor_master,titel_der_arbeit,datum_abschluss,note,person.name pname,vorname,kooperationspartner.name kname from graduierungsarbeit left join Studenten on graduierungsarbeit.person_id=Studenten.person_id left join Person on Studenten.person_id=person.person_id left join gehoert_zu on person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id left join ist_koop_von_ga on graduierungsarbeit.grad_id=ist_koop_von_ga.grad_id left join kooperationspartner on ist_koop_von_ga.koop_id=kooperationspartner.koop_id where graduierungsarbeit.bachelor_master='Bachelor'";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and datum_abschluss>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and datum_abschluss<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Graduierungsarbeiten (Bachelor)',req,data,['grad_id','Bachelor/Masster','Titel der Arbeit','Datum Abschluss','Note','Name','Vorname','Kooperationspartner'],['grad_id','bachelor_master','titel_der_arbeit','datum_abschluss','note','pname','vorname','kname']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_graduierungsarbeiten_ba_o_k',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select graduierungsarbeit.grad_id,bachelor_master,titel_der_arbeit,datum_abschluss,note,name,vorname from graduierungsarbeit left join Studenten on graduierungsarbeit.person_id=Studenten.person_id left join Person on Studenten.person_id=person.person_id left join gehoert_zu on person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id left join ist_koop_von_ga on graduierungsarbeit.grad_id=ist_koop_von_ga.grad_id where graduierungsarbeit.bachelor_master='Bachelor' and ist_koop_von_ga.grad_id is null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and datum_abschluss>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and datum_abschluss<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Graduierungsarbeiten (Bachelor ohne Kooperationspartner)',req,data,['grad_id','Bachelor/Masster','Titel der Arbeit','Datum Abschluss','Note','Name','Vorname'],['grad_id','bachelor_master','titel_der_arbeit','datum_abschluss','note','name','vorname']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_graduierungsarbeiten_ba_m_k',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select graduierungsarbeit.grad_id,bachelor_master,titel_der_arbeit,datum_abschluss,note,person.name pname,vorname,kooperationspartner.name kname from graduierungsarbeit left join Studenten on graduierungsarbeit.person_id=Studenten.person_id left join Person on Studenten.person_id=person.person_id left join gehoert_zu on person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id left join ist_koop_von_ga on graduierungsarbeit.grad_id=ist_koop_von_ga.grad_id left join kooperationspartner on ist_koop_von_ga.koop_id=kooperationspartner.koop_id where graduierungsarbeit.bachelor_master='Bachelor' and ist_koop_von_ga.grad_id is not null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and datum_abschluss>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and datum_abschluss<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Graduierungsarbeiten (Bachelor mit Kooperationspartner)',req,data,['grad_id','Bachelor/Masster','Titel der Arbeit','Datum Abschluss','Note','Prädikat','Name','Vorname','Kooperationspartner'],['grad_id','bachelor_master','titel_der_arbeit','datum_abschluss','note','pname','vorname','kname']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_graduierungsarbeiten_ma',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select graduierungsarbeit.grad_id,bachelor_master,titel_der_arbeit,datum_abschluss,note,person.name pname,vorname,kooperationspartner.name kname from graduierungsarbeit left join Studenten on graduierungsarbeit.person_id=Studenten.person_id left join Person on Studenten.person_id=person.person_id left join gehoert_zu on person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id left join ist_koop_von_ga on graduierungsarbeit.grad_id=ist_koop_von_ga.grad_id left join kooperationspartner on ist_koop_von_ga.koop_id=kooperationspartner.koop_id where graduierungsarbeit.bachelor_master='Master'";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and datum_abschluss>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and datum_abschluss<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Graduierungsarbeiten (Master)',req,data,['grad_id','Bachelor/Masster','Titel der Arbeit','Datum Abschluss','Note','Name','Vorname','Kooperationspartner'],['grad_id','bachelor_master','titel_der_arbeit','datum_abschluss','note','pname','vorname','kname']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_graduierungsarbeiten_ma_o_k',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select graduierungsarbeit.grad_id,bachelor_master,titel_der_arbeit,datum_abschluss,note,name,vorname from graduierungsarbeit left join Studenten on graduierungsarbeit.person_id=Studenten.person_id left join Person on Studenten.person_id=person.person_id left join gehoert_zu on person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id left join ist_koop_von_ga on graduierungsarbeit.grad_id=ist_koop_von_ga.grad_id where graduierungsarbeit.bachelor_master='Master' and ist_koop_von_ga.grad_id is null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and datum_abschluss>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and datum_abschluss<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Graduierungsarbeiten (Master ohne Kooperationspartner)',req,data,['grad_id','Bachelor/Masster','Titel der Arbeit','Datum Abschluss','Note','Name','Vorname'],['grad_id','bachelor_master','titel_der_arbeit','datum_abschluss','note','name','vorname']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_graduierungsarbeiten_ma_m_k',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select graduierungsarbeit.grad_id,bachelor_master,titel_der_arbeit,datum_abschluss,note,person.name pname,vorname,kooperationspartner.name kname from graduierungsarbeit left join Studenten on graduierungsarbeit.person_id=Studenten.person_id left join Person on Studenten.person_id=person.person_id left join gehoert_zu on person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id left join ist_koop_von_ga on graduierungsarbeit.grad_id=ist_koop_von_ga.grad_id left join kooperationspartner on ist_koop_von_ga.koop_id=kooperationspartner.koop_id where graduierungsarbeit.bachelor_master='Master' and ist_koop_von_ga.grad_id is not null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and datum_abschluss>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and datum_abschluss<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Graduierungsarbeiten (Master mit Kooperationspartner)',req,data,['grad_id','Bachelor/Masster','Titel der Arbeit','Datum Abschluss','Note','Name','Vorname','Kooperationspartner'],['grad_id','bachelor_master','titel_der_arbeit','datum_abschluss','note','pname','vorname','kname']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_wissen_veroeffentlichungen',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select wissenschaftliche_veroeffentlichung.ver_id,titel,verlag,verlagsort,autoren,jahr,identifier,sprache_der_publikation,publikation_im_rahmen_eines_projektes,typ_der_publikation,bandnummer_volume from Wissenschaftliche_Veroeffentlichung left join ist_autor_von on Wissenschaftliche_Veroeffentlichung.ver_id=ist_autor_von.ver_id left join Person on ist_autor_von.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id";
            sqlQuery = sqlQuery + " where organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                let year = new Date(req.session.date_begin);
                year = year.getFullYear();
                sqlQuery = sqlQuery + " and jahr>='" + year + "'";
            }

            if(req.session.date_end!="") {
                let year = new Date(req.session.date_end);
                year = year.getFullYear();
                sqlQuery = sqlQuery + " and jahr<='" + year + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Wissenschaftl. Veröffentlichungen',req,data,['ver_id','Titel','Verlag','Verlagsort','Autoren','Veröffentlichungsjahr','Identifier','Sprache der Publikation','Publikation entstand im Rahmen eines Projekts?','Typ der Publikation','Bandnummer/Volume'],['ver_id','titel','verlag','verlagsort','autoren','jahr','identifier','sprache_der_publikation','publikation_im_rahmen_eines_projektes','typ_der_publikation','bandnummer_volume']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_wissen_veroeffentlichungen_m_s',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select wissenschaftliche_veroeffentlichung.ver_id,titel,verlag,verlagsort,autoren,jahr,identifier,sprache_der_publikation,publikation_im_rahmen_eines_projektes,typ_der_publikation,bandnummer_volume,erlaeuterung,open_access from Wissenschaftliche_Veroeffentlichung left join Monografien_Beitraege_in_Sammelbaenden on Wissenschaftliche_Veroeffentlichung.ver_id=Monografien_Beitraege_in_Sammelbaenden.ver_id left join ist_autor_von on Wissenschaftliche_Veroeffentlichung.ver_id=ist_autor_von.ver_id left join Person on ist_autor_von.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id where Monografien_Beitraege_in_Sammelbaenden.ver_id is not null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                let year = new Date(req.session.date_begin);
                year = year.getFullYear();
                sqlQuery = sqlQuery + " and jahr>='" + year + "'";
            }

            if(req.session.date_end!="") {
                let year = new Date(req.session.date_end);
                year = year.getFullYear();
                sqlQuery = sqlQuery + " and jahr<='" + year + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Wissenschaftl. Veröffentlichungen (Monografien_Beiträge in Sammelbänden)',req,data,['ver_id','Titel','Verlag','Verlagsort','Autoren','Veröffentlichungsjahr','Identifier','Sprache der Publikation','Publikation entstand im Rahmen eines Projekts?','Typ der Publikation','Bandnummer/Volume','Erläuterung','Open Access?'],['ver_id','titel','verlag','verlagsort','autoren','jahr','identifier','sprache_der_publikation','publikation_im_rahmen_eines_projektes','typ_der_publikation','bandnummer_volume','erlaeuterung','open_access']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_wissen_veroeffentlichungen_w_z',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select wissenschaftliche_veroeffentlichung.ver_id,titel,verlag,verlagsort,autoren,jahr,identifier,sprache_der_publikation,publikation_im_rahmen_eines_projektes,typ_der_publikation,bandnummer_volume,mit_peer_review,open_access from Wissenschaftliche_Veroeffentlichung left join Fachbeitraege_in_wissen_Zeitschriften on Wissenschaftliche_Veroeffentlichung.ver_id=Fachbeitraege_in_wissen_Zeitschriften.ver_id left join ist_autor_von on Wissenschaftliche_Veroeffentlichung.ver_id=ist_autor_von.ver_id left join Person on ist_autor_von.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id where Fachbeitraege_in_wissen_Zeitschriften.ver_id is not null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                let year = new Date(req.session.date_begin);
                year = year.getFullYear();
                sqlQuery = sqlQuery + " and jahr>='" + year + "'";
            }

            if(req.session.date_end!="") {
                let year = new Date(req.session.date_end);
                year = year.getFullYear();
                sqlQuery = sqlQuery + " and jahr<='" + year + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Wissenschaftl. Veröffentlichungen (Fachbeiträge in wissenschaftl. Zeitschriften)',req,data,['ver_id','Titel','Verlag','Verlagsort','Autoren','Veröffentlichungsjahr','Identifier','Sprache der Publikation','Publikation entstand im Rahmen eines Projekts?','Typ der Publikation','Bandnummer/Volume','mit peer-review?','Open Access?'],['ver_id','titel','verlag','verlagsort','autoren','jahr','identifier','sprache_der_publikation','publikation_im_rahmen_eines_projektes','typ_der_publikation','bandnummer_volume','mit_peer_review','open_access']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_wissen_veroeffentlichungen_k',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select wissenschaftliche_veroeffentlichung.ver_id,titel,verlag,verlagsort,autoren,jahr,identifier,sprache_der_publikation,publikation_im_rahmen_eines_projektes,typ_der_publikation,bandnummer_volume,publikationsform,erlaeuterung from Wissenschaftliche_Veroeffentlichung left join kurzbeitraege on Wissenschaftliche_Veroeffentlichung.ver_id=kurzbeitraege.ver_id left join ist_autor_von on Wissenschaftliche_Veroeffentlichung.ver_id=ist_autor_von.ver_id left join Person on ist_autor_von.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id where kurzbeitraege.ver_id is not null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                let year = new Date(req.session.date_begin);
                year = year.getFullYear();
                sqlQuery = sqlQuery + " and jahr>='" + year + "'";
            }

            if(req.session.date_end!="") {
                let year = new Date(req.session.date_end);
                year = year.getFullYear();
                sqlQuery = sqlQuery + " and jahr<='" + year + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Wissenschaftl. Veröffentlichungen (Kurzbeiträge)',req,data,['ver_id','Titel','Verlag','Verlagsort','Autoren','Veröffentlichungsjahr','Identifier','Sprache der Publikation','Publikation entstand im Rahmen eines Projekts?','Typ der Publikation','Bandnummer/Volume','Publikationsform','Erläuterung'],['ver_id','titel','verlag','verlagsort','autoren','jahr','identifier','sprache_der_publikation','publikation_im_rahmen_eines_projektes','typ_der_publikation','bandnummer_volume','publikationsform','erlaeuterung']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_wissen_veroeffentlichungen_w_v',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select wissenschaftliche_veroeffentlichung.ver_id,titel,verlag,verlagsort,autoren,jahr,identifier,sprache_der_publikation,publikation_im_rahmen_eines_projektes,typ_der_publikation,bandnummer_volume,publikationsform,erlaeuterung,mit_peer_review,veroeffentlicht_im_rahmen_von from Wissenschaftliche_Veroeffentlichung left join veroeffentlichungen_im_Rahmen_von_wissen_Veranstaltungen on Wissenschaftliche_Veroeffentlichung.ver_id=veroeffentlichungen_im_Rahmen_von_wissen_Veranstaltungen.ver_id left join ist_autor_von on Wissenschaftliche_Veroeffentlichung.ver_id=ist_autor_von.ver_id left join Person on ist_autor_von.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id where veroeffentlichungen_im_Rahmen_von_wissen_Veranstaltungen.ver_id is not null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                let year = new Date(req.session.date_begin);
                year = year.getFullYear();
                sqlQuery = sqlQuery + " and jahr>='" + year + "'";
            }

            if(req.session.date_end!="") {
                let year = new Date(req.session.date_end);
                year = year.getFullYear();
                sqlQuery = sqlQuery + " and jahr<='" + year + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Wissenschaftl. Veröffentlichungen (ihm Rahmen von wissenschaftl. Veranstaltungen)',req,data,['ver_id','Titel','Verlag','Verlagsort','Autoren','Veröffentlichungsjahr','Identifier','Sprache der Publikation','Publikation entstand im Rahmen eines Projekts?','Typ der Publikation','Bandnummer/Volume','Publikationsform','Erläuterung','mit peer-review?','veröffentlicht ihm Rahmen von'],['ver_id','titel','verlag','verlagsort','autoren','jahr','identifier','sprache_der_publikation','publikation_im_rahmen_eines_projektes','typ_der_publikation','bandnummer_volume','publikationsform','erlaeuterung','mit_peer_review','veroeffetlicht_im_rahmen_von']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_wissen_veroeffentlichungen_s_v',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select wissenschaftliche_veroeffentlichung.ver_id,titel,verlag,verlagsort,autoren,jahr,identifier,sprache_der_publikation,publikation_im_rahmen_eines_projektes,typ_der_publikation,bandnummer_volume from Wissenschaftliche_Veroeffentlichung left join Fachbeitraege_in_wissen_Zeitschriften on Wissenschaftliche_Veroeffentlichung.ver_id=Fachbeitraege_in_wissen_Zeitschriften.ver_id left join veroeffentlichungen_im_Rahmen_von_wissen_Veranstaltungen on Wissenschaftliche_Veroeffentlichung.ver_id=veroeffentlichungen_im_Rahmen_von_wissen_Veranstaltungen.ver_id left join kurzbeitraege on Wissenschaftliche_Veroeffentlichung.ver_id=kurzbeitraege.ver_id left join Monografien_Beitraege_in_Sammelbaenden on Wissenschaftliche_Veroeffentlichung.ver_id=Monografien_Beitraege_in_Sammelbaenden.ver_id left join ist_autor_von on Wissenschaftliche_Veroeffentlichung.ver_id=ist_autor_von.ver_id left join Person on ist_autor_von.person_id=Person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id where Fachbeitraege_in_wissen_Zeitschriften.ver_id is null and veroeffentlichungen_im_Rahmen_von_wissen_Veranstaltungen.ver_id is null and kurzbeitraege.ver_id is null and Monografien_Beitraege_in_Sammelbaenden.ver_id is null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                let year = new Date(req.session.date_begin);
                year = year.getFullYear();
                sqlQuery = sqlQuery + " and jahr>='" + year + "'";
            }

            if(req.session.date_end!="") {
                let year = new Date(req.session.date_end);
                year = year.getFullYear();
                sqlQuery = sqlQuery + " and jahr<='" + year + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Wissenschaftl. Veröffentlichungen (sonstige)',req,data,['ver_id','Titel','Verlag','Verlagsort','Autoren','Veröffentlichungsjahr','Identifier','Sprache der Publikation','Publikation entstand im Rahmen eines Projekts?','Typ der Publikation','Bandnummer/Volume'],['ver_id','titel','verlag','verlagsort','autoren','jahr','identifier','sprache_der_publikation','publikation_im_rahmen_eines_projektes','typ_der_publikation','bandnummer_volume']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_promotionsverfahren',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select promotionsverfahren.pro_id,titel_des_promotionsvorhabens,kooperativ,offizieller_beginn_des_promotionsvorhabens,arbeitsplatz_bzw_ueberwieg_durchfuehrung_der_forschungsarbeit,abschluss_betreuungsvereinbarung_mit_datum,art_der_finanzierung,koop_universitaet,koop_professor from Promotionsverfahren left join Professoren on Promotionsverfahren.person_id_prof=Professoren.person_id left join Person on Professoren.person_id=person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id";
            sqlQuery = sqlQuery + " where organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and offizieller_beginn_des_promotionsvorhabens>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and offizieller_beginn_des_promotionsvorhabens<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Promotionsverfahren',req,data,['pro_id','Titel des Promotionsvorhabens','kooperativ?','offizieller Beginn','Arbeitsplatz bzw überwiegende Durchführung der Forschungsarbeit','Abschluss Betreuungsvereinbarung mit Datum','Art der Finanzierung ','koop. Universität','koop. Fakultät','koop. Professor'],['pro_id','titel_des_promotionsvorhabens','kooperativ','offizieller_beginn_des_promotionsvorhabens','arbeitsplatz_bzw_ueberwieg_durchfuehrung_der_forschungsarbeit','abschluss_betreuungsvereinbarung_mit_datum','art_der_finanzierung','koop_universitaet','koop_fakultaet','koop_professor']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_promotionsverfahren_a',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select abgeschlossene_promotionsverfahren.pro_id,titel_des_promotionsvorhabens,kooperativ,offizieller_beginn_des_promotionsvorhabens,arbeitsplatz_bzw_ueberwieg_durchfuehrung_der_forschungsarbeit,abschluss_betreuungsvereinbarung_mit_datum,art_der_finanzierung,koop_universitaet,koop_professor,publikationsdaten,datum_abschluss,praedikat,belegexemplar_an_HTWK_bibo_gegeben from abgeschlossene_Promotionsverfahren left join Promotionsverfahren on abgeschlossene_Promotionsverfahren.pro_id=Promotionsverfahren.pro_id left join Professoren on Promotionsverfahren.person_id_prof=Professoren.person_id left join Person on Professoren.person_id=person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id";
            sqlQuery = sqlQuery + " where organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and offizieller_beginn_des_promotionsvorhabens>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and offizieller_beginn_des_promotionsvorhabens<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Promotionsverfahren (abgeschlossen)',req,data,['pro_id','Titel des Promotionsvorhabens','kooperativ?','offizieller Beginn','Arbeitsplatz bzw überwiegende Durchführung der Forschungsarbeit','Abschluss Betreuungsvereinbarung mit Datum','Art der Finanzierung ','koop. Universität','koop. Fakultät','koop. Professor','Publikationsdaten','Datum Abschluss','Prädikat','Belegexemplar an HTWK Bibliothek gegeben?'],['pro_id','titel_des_promotionsvorhabens','kooperativ','offizieller_beginn_des_promotionsvorhabens','arbeitsplatz_bzw_ueberwieg_durchfuehrung_der_forschungsarbeit','abschluss_betreuungsvereinbarung_mit_datum','art_der_finanzierung','koop_universitaet','koop_fakultaet','koop_professor','publikationsdaten','datum_abschluss','praedikat','belegexemplar_an_HTWK_bibo_gegeben']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_promotionsverfahren_l',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select laufende_promotionsverfahren.pro_id,titel_des_promotionsvorhabens,kooperativ,offizieller_beginn_des_promotionsvorhabens,arbeitsplatz_bzw_ueberwieg_durchfuehrung_der_forschungsarbeit,abschluss_betreuungsvereinbarung_mit_datum,art_der_finanzierung,koop_universitaet,koop_professor,geplanter_abschluss from laufende_Promotionsverfahren left join Promotionsverfahren on laufende_Promotionsverfahren.pro_id=Promotionsverfahren.pro_id left join Professoren on Promotionsverfahren.person_id_prof=Professoren.person_id left join Person on Professoren.person_id=person.person_id left join gehoert_zu on Person.person_id=gehoert_zu.person_id left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id";
            sqlQuery = sqlQuery + " where organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and offizieller_beginn_des_promotionsvorhabens>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and offizieller_beginn_des_promotionsvorhabens<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Promotionsverfahren (laufend)',req,data,['pro_id','Titel des Promotionsvorhabens','kooperativ?','offizieller Beginn','Arbeitsplatz bzw überwiegende Durchführung der Forschungsarbeit','Abschluss Betreuungsvereinbarung mit Datum','Art der Finanzierung ','koop. Universität','koop. Fakultät','koop. Professor','geplanter Abschluss'],['pro_id','titel_des_promotionsvorhabens','kooperativ','offizieller_beginn_des_promotionsvorhabens','arbeitsplatz_bzw_ueberwieg_durchfuehrung_der_forschungsarbeit','abschluss_betreuungsvereinbarung_mit_datum','art_der_finanzierung','koop_universitaet','koop_fakultaet','koop_professor','geplanter_abschluss']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_wissen_veranstaltung',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select wissenschaftliche_veranstaltung.wissen_v_id,titel_der_veranstaltung,datum_beginn,datum_ende,art_der_veranstaltung,erlaeuterung,ort,land from Wissenschaftliche_Veranstaltung left join beteiligt_an on Wissenschaftliche_Veranstaltung.wissen_v_id=beteiligt_an.wissen_v_id left join Organisationseinheit on beteiligt_an.org_id=Organisationseinheit.org_id where beteiligt_an.org_id is not null";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and datum_beginn>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and datum_beginn<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Hochschulinterne wissenschaftliche Veranstaltungen',req,data,['wissen_v_id','Titel der Veranstaltung','Datum Beginn','Datum Ende','Art der Veranstaltung','Erläuterung','Ort','Land'],['wissen_v_id','titel_der_veranstaltung','datum_beginn','datum_ende','art_der_veranstaltung','erlaeuterung','ort','land']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_wissen_veranstaltung_ft',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select wissenschaftliche_veranstaltung.wissen_v_id,titel_der_veranstaltung,datum_beginn,datum_ende,art_der_veranstaltung,erlaeuterung,ort,land from Wissenschaftliche_Veranstaltung left join beteiligt_an on Wissenschaftliche_Veranstaltung.wissen_v_id=beteiligt_an.wissen_v_id left join Organisationseinheit on beteiligt_an.org_id=Organisationseinheit.org_id where beteiligt_an.org_id is not null and Wissenschaftliche_Veranstaltung.art_der_veranstaltung='Fachtagung'";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and datum_beginn>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and datum_beginn<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Hochschulinterne wissenschaftliche Veranstaltungen (Fachtagungen)',req,data,['wissen_v_id','Titel der Veranstaltung','Datum Beginn','Datum Ende','Art der Veranstaltung','Erläuterung','Ort','Land'],['wissen_v_id','titel_der_veranstaltung','datum_beginn','datum_ende','art_der_veranstaltung','erlaeuterung','ort','land']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_wissen_veranstaltung_fm',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select wissenschaftliche_veranstaltung.wissen_v_id,titel_der_veranstaltung,datum_beginn,datum_ende,art_der_veranstaltung,erlaeuterung,ort,land from Wissenschaftliche_Veranstaltung left join beteiligt_an on Wissenschaftliche_Veranstaltung.wissen_v_id=beteiligt_an.wissen_v_id left join Organisationseinheit on beteiligt_an.org_id=Organisationseinheit.org_id where beteiligt_an.org_id is not null and Wissenschaftliche_Veranstaltung.art_der_veranstaltung='Fachmesse'";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and datum_beginn>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and datum_beginn<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Hochschulinterne wissenschaftliche Veranstaltungen (Fachmessen)',req,data,['wissen_v_id','Titel der Veranstaltung','Datum Beginn','Datum Ende','Art der Veranstaltung','Erläuterung','Ort','Land'],['wissen_v_id','titel_der_veranstaltung','datum_beginn','datum_ende','art_der_veranstaltung','erlaeuterung','ort','land']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/show_table_wissen_veranstaltung_sv',function(req,res) {
    let sql = require("mssql");
    var pool = new sql.ConnectionPool(req.session.connectionString);

    pool.connect(err => {
        if(err) {
            console.log(err);
            res.send(err);
            sql.close;
        } else {
            let sqlRequest = new sql.Request(pool);
            

            let sqlQuery = "select wissenschaftliche_veranstaltung.wissen_v_id,titel_der_veranstaltung,datum_beginn,datum_ende,art_der_veranstaltung,erlaeuterung,ort,land from Wissenschaftliche_Veranstaltung left join beteiligt_an on Wissenschaftliche_Veranstaltung.wissen_v_id=beteiligt_an.wissen_v_id left join Organisationseinheit on beteiligt_an.org_id=Organisationseinheit.org_id where beteiligt_an.org_id is not null and Wissenschaftliche_Veranstaltung.art_der_veranstaltung='sonstige Veranstaltung'";
            sqlQuery = sqlQuery + " and organisationseinheit.org_id=" + req.session.organisationseinheit_id;
            
            if(req.session.date_begin!="") {
                sqlQuery = sqlQuery + " and datum_beginn>='" + req.session.date_begin + "'";
            }

            if(req.session.date_end!="") {
                sqlQuery = sqlQuery + " and datum_beginn<='" + req.session.date_end + "'";
            }
            sqlQuery = sqlQuery + ";";

            console.log(sqlQuery);
            sqlRequest.query(sqlQuery).then(data => {

                if (err) {
                    console.log(err);
                } else {
                    console.table(data.recordset);
                    res.send(createTable('Hochschulintern wissenschaftliche Veranstaltungen (sontige Veranstaltungen)',req,data,['wissen_v_id','Titel der Veranstaltung','Datum Beginn','Datum Ende','Art der Veranstaltung','Erläuterung','Ort','Land'],['wissen_v_id','titel_der_veranstaltung','datum_beginn','datum_ende','art_der_veranstaltung','erlaeuterung','ort','land']));
                }
                sql.close; 
            });
        }
    })
});

app.get('/download', function(req,res) {
    var file = __dirname + '/CSV_files/' + req.session.csv_name + '_' + req.session.date_begin + '_' + req.session.date_end + '.csv';
    res.download(file); 
});


//Erstellt den HTML-Code für die Darstellung der Tabellen

function createTable(tablename,req,data,tablehead,attributenames) {

    var h,th,style,row,htmlcode,meta,csv_string;
    
    style = '<style>button {font-size: 20px; border: 3px solid black; width: 250px; height: 50px;} h1 {background:black;color:whitesmoke;border:20px solid black;} table,th,td {border:1px solid black;border-collapse: collapse;} th,td {padding:15px;}th {vertical-align: bottom;background-color: #666;color: #fff;}th {position: -webkit-sticky;position: sticky;top: 0;z-index: 2;}</style>';
    h = '<img height="100px" width="700px" src="http://localhost:5000/images/HTWK_Zusatz_de_H_Black.jpg" alt="HTWK_Logo_Black">';
    h = h + '<h1>' + tablename + '</h1>';
    
    meta = '<p>Organisationseinheit: ' + req.session.organisationseinheit_name + '</p>' ;
    meta = meta + '<p>Zeitraum: ' + req.session.date_begin + ' - ' + req.session.date_end + '</p>';
    
    th = '<table><tr>';
    csv_string = '';

    for(let j = 0;j<tablehead.length;j++) {
       th = th + '<th>' + tablehead[j] + '</th>';
       csv_string = csv_string + tablehead[j] + ';'
    }
    th = th + '</tr>';
    csv_string = csv_string + '\n';

    console.log('th: ' + th);
    row = "";
    for(let j = 0;j<data.recordset.length;j++) {
        row = row + '<tr>';
        for (let k = 0;k<attributenames.length;k++) {
            if(eval('data.recordset[j].' + attributenames[k]) !=null) {
                row = row + '<td>' + eval('data.recordset[j].' + attributenames[k]) + '</td>';
                csv_string = csv_string + eval('data.recordset[j].' + attributenames[k]) + ';';
            } else {
                row = row + '<td></td>';
                csv_string = csv_string + ';';
            }
        }
        row = row + '</tr>';
        csv_string = csv_string + '\n';
    }
    console.log("row: " + row);
    req.session.csv_name = tablename;
    fs.writeFileSync('CSV_files/' + tablename + '_' + req.session.date_begin + '_' + req.session.date_end + '.csv', csv_string, function (err) {console.log('In Datei geschrieben!');});

    return htmlcode = style + h + meta + th + row + '</table></br></br><form action="/download" method="get"><button id="download_button">Download CSV</button></form></br><button onclick="history.go(-1)">Zurück</button>';
}

//Starten den Server

server.listen(5000, function() {
    console.log('Node Web Server is running...');
});



