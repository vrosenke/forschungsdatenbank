create table Person (
			person_id int not null primary key,
			geschlecht varchar(10) not null,
			name varchar(50) not null,
			vorname varchar(50) not null,
			staatsangehoerigkeit varchar(50),
			geburtsdatum date,
			akademischer_grad varchar(30)
			check (
				geschlecht='m' or geschlecht='w' or geschlecht='n'
			)
);

create table Promovenden ( 
			person_id int not null primary key references person(person_id),
			fachgebiet varchar(50) not null,
);

create table Mitarbeiter (
			person_id int not null primary key references person(person_id),
			mitarbeiter_id int,
			taetigkeitsbereich varchar(50) not null
);

create table Professoren (
			person_id int not null primary key references person(person_id),
			berufungsgebiet varchar(50) not null
);



create table Studenten (
			person_id int not null primary key references person(person_id),
			matrikelnummer int not null,
			matrikel varchar(10) not null
);

create table Promotionsverfahren (
			pro_id int not null primary key,
			titel_des_promotionsvorhabens varchar(100) not null,
			kooperativ varchar(4) not null,
			offizieller_beginn_des_promotionsvorhabens date not null,
			arbeitsplatz_bzw_ueberwieg_durchfuehrung_der_forschungsarbeit varchar(100),
			abschluss_betreuungsvereinbarung_mit_datum varchar(50),
			art_der_finanzierung varchar(50) not null,
			koop_universitaet varchar(50),
			koop_fakultaet varchar(50),
			koop_professor varchar (30),
			person_id_prom int references promovenden(person_id),
			person_id_prof int references professoren(person_id)
			check (
				(kooperativ='ja' or kooperativ='nein') and (art_der_finanzierung='sonstige' or art_der_finanzierung='Arbeitsvertrag' or art_der_finanzierung='Stipendium' or art_der_finanzierung='Arbeitsvertrag/Stipendium')
			)
);

create table abgeschlossene_Promotionsverfahren (
			pro_id int not null primary key references promotionsverfahren(pro_id),
			publikationsdaten varchar(50),
			datum_abschluss date not null,
			praedikat varchar(10) not null,
			belegexemplar_an_HTWK_bibo_gegeben varchar(4)
			check (
				(praedikat='summa cum laude' or praedikat='magna cum laude' or praedikat='cum laude' or praedikat='satis bene' or praedikat='rite' or praedikat='non probatum') and (belegexemplar_an_HTWK_bibo_gegeben='ja' or belegexemplar_an_HTWK_bibo_gegeben='nein')
			)
);

create table laufende_Promotionsverfahren (
			pro_id int not null primary key references promotionsverfahren(pro_id),
			geplanter_abschluss date not null
);

create table Gremientaetigkeit (
			mwgg_id int not null primary key,
			beschreibung_der_taetigkeit varchar(100) not null,
			gremium_einrichtung varchar(50) not null,
			datum_beginn date not null,
			funktion varchar(50) not null,
 			erlaeuterung varchar(300),
			ort varchar(30) not null,
			land varchar(30) not null,
			person_id int references professoren(person_id)
);

create table gastwissenschaftlertaetigkeit (
			gw_id int not null primary key,
			arbeitsthema_anlass varchar(100) not null,
			foerderprogramm varchar(100),
			datum_beginn date not null,
			datum_ende date,
			anmerkung varchar(300),
			aufnehmende_einrichtung varchar(100),
			person_id_ma int references mitarbeiter(person_id),
			person_id_prof int references professoren(person_id),
			check (
				(person_id_ma is not null and person_id_prof is null) or (person_id_ma is null and person_id_prof is not null)
			)
);


create table graduierungsarbeit (
			grad_id int not null primary key,
			bachelor_master varchar(10) not null,
			titel_der_arbeit varchar(100) not null,
			datum_abschluss date not null,
			note numeric(3,1),
			person_id int references studenten(person_id)
			check (
				(bachelor_master='Bachelor' or bachelor_master='Master') and (note=1.0 or note=1.3 or note=1.7 or note=2.0 or note=2.3 or note=2.7 or note=3.0 or note=3.3 or note=3.7 or note=4.0)
			)
);


create table ist_gutachter_von_ga (
			grad_id int not null references graduierungsarbeit(grad_id),
			person_id int not null references professoren(person_id),
			primary key(grad_id, person_id)
);

create table Kooperationspartner (
			koop_id int not null primary key,
			name varchar(50) not null,
			adresse varchar(50),
			ansprechpartner varchar(50)
);

create table ist_koop_von_ga (
			grad_id int not null primary key references graduierungsarbeit(grad_id),
			koop_id int not null references kooperationspartner(koop_id),
);

create table fue_projekte (
			fue_id int not null primary key,
			projekttitel_deutsch varchar(100) not null,
			projekttitel_englisch varchar(100) not null,
			projektart varchar(50) not null,
			beginn date not null,
			datum_abschluss date,
			kurzbeschreibung_deutsch varchar(300) not null,
			kurzbeschreibung_englisch varchar(300) not null,
			person_id int references professoren(person_id)
);

create table ist_koop_von_proj (
			koop_id int not null references kooperationspartner(koop_id),
			fue_id int not null references fue_projekte(fue_id),
			primary key (koop_id, fue_id)
);

create table ist_beteiligt (
			fue_id int not null references fue_projekte(fue_id),
			person_id int not null references person(person_id),
			primary key (fue_id, person_id)
);

create table Wissenschaftliche_Veranstaltung (
			wissen_v_id int not null primary key,
			titel_der_veranstaltung varchar(100) not null,
			datum_beginn date not null,
			datum_ende date not null,
			art_der_veranstaltung varchar(50) not null,
			erlaeuterung varchar(300) not null,
			ort varchar(30) not null,
			land varchar(30) not null
			check (
				art_der_veranstaltung='sonstige' or art_der_veranstaltung='Fachtagung' or art_der_veranstaltung='Fachmesse' or art_der_veranstaltung='Ausstellung'
			)
);

create table leitet_wiss_V (
			person_id int not null references professoren(person_id),
			wissen_v_id int not null references wissenschaftliche_veranstaltung(wissen_v_id),
			primary key (person_id, wissen_v_id)
);

create table durchgefuehrt_organisiert (
			wissen_v_id int not null references wissenschaftliche_veranstaltung(wissen_v_id),
 		 	person_id int not null references person(person_id),
			primary key (wissen_v_id, person_id)
);

create table wissenschaftliche_vortraege (
			vortr_id int not null primary key,
			titel varchar(50) not null,
			datum date not null,
			ort varchar(30) not null,
			land varchar(30) not null,
			erlaeuterung varchar(300) not null,
			co_vortragende varchar(300)
);

create table haelt (
			vortr_id int not null references wissenschaftliche_vortraege(vortr_id),
			person_id int not null references person(person_id),
			primary key (vortr_id, person_id)
);

create table gehalten_im_Rahmen_von (
			vortr_id int not null primary key references wissenschaftliche_vortraege(vortr_id),
			wissen_v_id int not null references wissenschaftliche_veranstaltung(wissen_v_id)
);

create table Organisationseinheit (
			org_id int not null primary key,
			name_der_organisationseinheit varchar(100) not null,
			kurzbeschreibung varchar(300)
);

create table ist_untergeordnet (
			org_id int not null primary key references organisationseinheit(org_id),
			org_id_untergeordnet int not null references organisationseinheit(org_id)
); 

create table gehoert_zu (
			person_id int not null primary key references person(person_id),
			org_id int not null references organisationseinheit(org_id),
);

create table beteiligt_an (
			wissen_v_id int not null primary key references wissenschaftliche_veranstaltung(wissen_v_id),
			org_id int not null references organisationseinheit(org_id)
);

create table auszeichnung (
			af_id int not null primary key,
			titel_der_auszeichnung varchar(50) not null,
			art_des_preises varchar(50) not null,
			datum date not null,
			erlaeuterung varchar(300),
			ort varchar(30) not null,
			land varchar(30) not null,
			auszeichnung_ist_dotiert varchar(4) not null,
			vergeben_durch varchar(50) not null,
			person_id int references person(person_id)
			check (
				auszeichnung_ist_dotiert='ja' or auszeichnung_ist_dotiert='nein'
			)
);

create table Schutzrechte (
			s_id int not null primary key,
			titel varchar(100) not null,
			status varchar(50) not null,
			datum_der_anmeldung date not null,
			kennzeichen_veroeffentlichungsnummer varchar(30),
			person_id int references person(person_id)
);

create table Existenzgruendung (
			ex_id int not null primary key,
			name_der_gruendung varchar(50) not null,
			ort varchar(30) not null,
			gruendungsdatum date not null,
			rechtsform varchar(30) not null,
			status_der_gruendung varchar(30) not null
			check (
				rechtsform='GmbH' or rechtsform='AG' or rechtsform='OHG' or rechtsform='KG' or rechtsform='GbR' or rechtsform='Einzelunternehmen'
			)
);

create table gruendet_aus (
			ex_id int not null references existenzgruendung(ex_id),
			person_id int not null references person(person_id),
			primary key (ex_id, person_id)
);

create table Wissenschaftliche_Veroeffentlichung (
			ver_id int not null primary key,
			titel varchar(300) not null,
			verlag varchar(50),
			verlagsort varchar(30),
			autoren varchar(300),
			jahr smallint not null,
			identifier varchar(100),
			sprache_der_publikation varchar(30) not null,
			publikation_im_rahmen_eines_projektes varchar(4),
			typ_der_publikation varchar(100) not null,
			bandnummer_volume int
			check (
				(sprache_der_publikation='Deutsch' or sprache_der_publikation='Englisch') and (publikation_im_rahmen_eines_projektes='ja' or publikation_im_rahmen_eines_projektes='nein') and (typ_der_publikation='Tagungsband' or typ_der_publikation='Kurzbeitrag' or typ_der_publikation='sonstige' or typ_der_publikation='Fachbeitrag_in_wissenschaftlicher_Zeitschrift' or typ_der_publikation='Monografien/Beitraege in Sammelbaenden')
			)
);

create table Monografien_Beitraege_in_Sammelbaenden (
			ver_id int not null primary key references wissenschaftliche_veroeffentlichung(ver_id),
			erlaeuterung varchar(300),
			open_access varchar(4) not null
			check (
				open_access='ja' or open_access='nein'
			)
);

create table kurzbeitraege (
			ver_id int not null primary key references wissenschaftliche_veroeffentlichung(ver_id),
			publikationsform varchar(50) not null,
			erlaeuterung varchar(300) 
);

create table Fachbeitraege_in_wissen_Zeitschriften (
			ver_id int not null primary key references wissenschaftliche_veroeffentlichung(ver_id),
			mit_peer_review varchar(4) not null,
			open_access varchar(4) not null
			check (
				(open_access='ja' or open_access='nein') and (mit_peer_review='ja' or mit_peer_review='nein')
			)
);

create table veroeffentlichungen_im_Rahmen_von_wissen_Veranstaltungen (
			ver_id int not null primary key references wissenschaftliche_veroeffentlichung(ver_id),
			publikationsform varchar(50) not null,
			erlaeuterung varchar(300),
			mit_peer_review varchar(4) not null,
			veroeffentlicht_im_rahmen_von varchar(300)
			check (
				mit_peer_review='ja' or mit_peer_review='nein'
			)
);

create table ist_autor_von (
			ver_id int not null references wissenschaftliche_veroeffentlichung(ver_id),
			person_id int not null references person(person_id),
			primary key (ver_id, person_id)
);

create table aufgenommene_gastwissenschaftler (
			agw_id int not null primary key,
			name varchar(50) not null,
			vorname varchar(50) not null,
			arbeitsthema varchar(100) not null,
			datum_beginn date not null,
			datum_ende date,
			entsendende_einrichtung varchar(100),
			person_id int references professoren(person_id)
);


--------------------------------------
--------------------------------------
--Ausgabe

select * from abgeschlossene_Promotionsverfahren;
select * from aufgenommene_gastwissenschaftler;
select * from auszeichnung;
select * from beteiligt_an;
select * from durchgefuehrt_organisiert;
select * from Existenzgruendung;
select * from Fachbeitraege_in_wissen_Zeitschriften;
select * from fue_projekte;
select * from gastwissenschaftlertaetigkeit;
select * from gehalten_im_Rahmen_von;
select * from gehoert_zu;
select * from graduierungsarbeit;
select * from Gremientaetigkeit;
select * from gruendet_aus;
select * from haelt;
select * from ist_autor_von;
select * from ist_beteiligt;
select * from ist_gutachter_von_ga;
select * from ist_gutachter_von_Pro;
select * from ist_koop_von_ga;
select * from ist_koop_von_proj;
select * from ist_untergeordnet;
select * from Kooperationspartner;
select * from kurzbeitraege;
select * from laufende_Promotionsverfahren;
select * from leitet_wiss_V;
select * from Mitarbeiter;
select * from Monografien_Beitraege_in_Sammelbaenden;
select * from Organisationseinheit;
select * from Person;
select * from Professoren;
select * from Promotionsverfahren;
select * from Promovenden;
select * from Schutzrechte;
select * from Studenten;
select * from veroeffentlichungen_im_Rahmen_von_wissen_Veranstaltungen;
select * from Wissenschaftliche_Veranstaltung;
select * from Wissenschaftliche_Veroeffentlichung;
select * from wissenschaftliche_vortraege;
-------------------------------------------------------------
-------------------------------------------------------------
--Inhalte löschen
delete from abgeschlossene_Promotionsverfahren;
delete from aufgenommene_gastwissenschaftler;
delete from auszeichnung;
delete from beteiligt_an;
delete from durchgefuehrt_organisiert;
delete from Existenzgruendung;
delete from Fachbeitraege_in_wissen_Zeitschriften;
delete from fue_projekte;
delete from gastwissenschaftlertaetigkeit;
delete from gehalten_im_Rahmen_von;
delete from gehoert_zu;
delete from graduierungsarbeit;
delete from Gremientaetigkeit;
delete from gruendet_aus;
delete from haelt;
delete from ist_autor_von;
delete from ist_beteiligt;
delete from ist_gutachter_von_ga;
delete from ist_gutachter_von_Pro;
delete from ist_koop_von_ga;
delete from ist_koop_von_proj;
delete from ist_untergeordnet;
delete from Kooperationspartner;
delete from kurzbeitraege;
delete from laufende_Promotionsverfahren;
delete from leitet_wiss_V;
delete from Mitarbeiter;
delete from Monografien_Beitraege_in_Sammelbaenden;
delete from Organisationseinheit;
delete from Person;
delete from Professoren;
delete from Promotionsverfahren;
delete from Promovenden;
delete from Schutzrechte;
delete from Studenten;
delete from veroeffentlichungen_im_Rahmen_von_wissen_Veranstaltungen;
delete from Wissenschaftliche_Veranstaltung;
delete from Wissenschaftliche_Veroeffentlichung;
delete from wissenschaftliche_vortraege;

--Bsp Anfragen

select grad_id, bachelor_master, titel_der_arbeit, datum_abschluss, name_der_organisationseinheit,name,vorname from graduierungsarbeit
left join Studenten on graduierungsarbeit.person_id=Studenten.person_id
left join Person on Studenten.person_id=person.person_id
left join gehoert_zu on person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id
where Organisationseinheit.name_der_organisationseinheit='Fakultaet Informatik und Medien' and graduierungsarbeit.datum_abschluss>='01.01.2020' and graduierungsarbeit.datum_abschluss<='31.03.2021' and bachelor_master='Bachelor';

--Anfragen für Auswertung
--Promotionsverfahren
select count(*) from Promotionsverfahren
left join Professoren on Promotionsverfahren.person_id_prof=Professoren.person_id
left join Person on Professoren.person_id=person.person_id
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id;

--abgeschlossene Promotionsverfahren
select count(*) from abgeschlossene_Promotionsverfahren 
left join Promotionsverfahren on abgeschlossene_Promotionsverfahren.pro_id=Promotionsverfahren.pro_id
left join Professoren on Promotionsverfahren.person_id_prof=Professoren.person_id
left join Person on Professoren.person_id=person.person_id
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id;

--laufende Promotionsverfahren
select count(*) from laufende_Promotionsverfahren 
left join Promotionsverfahren on laufende_Promotionsverfahren.pro_id=Promotionsverfahren.pro_id
left join Professoren on Promotionsverfahren.person_id_prof=Professoren.person_id
left join Person on Professoren.person_id=person.person_id
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id;


--Graduierungsarbeiten
select count(*) from graduierungsarbeit
left join Studenten on graduierungsarbeit.person_id=Studenten.person_id
left join Person on Studenten.person_id=person.person_id
left join gehoert_zu on person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id
left join ist_koop_von_ga on graduierungsarbeit.grad_id=ist_koop_von_ga.grad_id;

--Graduierungsarbeiten(Bachelor)
select * from graduierungsarbeit
left join Studenten on graduierungsarbeit.person_id=Studenten.person_id
left join Person on Studenten.person_id=person.person_id
left join gehoert_zu on person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id
left join ist_koop_von_ga on graduierungsarbeit.grad_id=ist_koop_von_ga.grad_id
where graduierungsarbeit.bachelor_master='Bachelor';

--ohne Kooperationspartner
select count(*) from graduierungsarbeit
left join Studenten on graduierungsarbeit.person_id=Studenten.person_id
left join Person on Studenten.person_id=person.person_id
left join gehoert_zu on person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id
left join ist_koop_von_ga on graduierungsarbeit.grad_id=ist_koop_von_ga.grad_id
where graduierungsarbeit.bachelor_master='Bachelor' and ist_koop_von_ga.grad_id is null;

--mit Kooperationspartner
select count(*) from graduierungsarbeit
left join Studenten on graduierungsarbeit.person_id=Studenten.person_id
left join Person on Studenten.person_id=person.person_id
left join gehoert_zu on person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id
left join ist_koop_von_ga on graduierungsarbeit.grad_id=ist_koop_von_ga.grad_id
where graduierungsarbeit.bachelor_master='Bachelor' and ist_koop_von_ga.grad_id is not null;

--Graduierungsarbeit(Master)
select count(*) from graduierungsarbeit
left join Studenten on graduierungsarbeit.person_id=Studenten.person_id
left join Person on Studenten.person_id=person.person_id
left join gehoert_zu on person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id
left join ist_koop_von_ga on graduierungsarbeit.grad_id=ist_koop_von_ga.grad_id
where graduierungsarbeit.bachelor_master='Master';

--ohne Kooperationspartner
select count(*) from graduierungsarbeit
left join Studenten on graduierungsarbeit.person_id=Studenten.person_id
left join Person on Studenten.person_id=person.person_id
left join gehoert_zu on person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id
left join ist_koop_von_ga on graduierungsarbeit.grad_id=ist_koop_von_ga.grad_id
where graduierungsarbeit.bachelor_master='Master' and ist_koop_von_ga.grad_id is null;

--mit Kooperationspartner
select count(*) from graduierungsarbeit
left join Studenten on graduierungsarbeit.person_id=Studenten.person_id
left join Person on Studenten.person_id=person.person_id
left join gehoert_zu on person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id
left join ist_koop_von_ga on graduierungsarbeit.grad_id=ist_koop_von_ga.grad_id
where graduierungsarbeit.bachelor_master='Master' and ist_koop_von_ga.grad_id is not null;


--Wissenschaftliche Veröffentlichungen
select count(*) from Wissenschaftliche_Veroeffentlichung
left join ist_autor_von on Wissenschaftliche_Veroeffentlichung.ver_id=ist_autor_von.ver_id
left join Person on ist_autor_von.person_id=Person.person_id
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id

--davon Monografien/Beiträge in Sammelbänden
select count(*) from Wissenschaftliche_Veroeffentlichung
left join Monografien_Beitraege_in_Sammelbaenden on Wissenschaftliche_Veroeffentlichung.ver_id=Monografien_Beitraege_in_Sammelbaenden.ver_id
left join ist_autor_von on Wissenschaftliche_Veroeffentlichung.ver_id=ist_autor_von.ver_id
left join Person on ist_autor_von.person_id=Person.person_id
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id
where Monografien_Beitraege_in_Sammelbaenden.ver_id is not null;

--davon Fachbeiträge in wissenschaftl Zeitschriften
select count(*) from Wissenschaftliche_Veroeffentlichung
left join Fachbeitraege_in_wissen_Zeitschriften on Wissenschaftliche_Veroeffentlichung.ver_id=Fachbeitraege_in_wissen_Zeitschriften.ver_id
left join ist_autor_von on Wissenschaftliche_Veroeffentlichung.ver_id=ist_autor_von.ver_id
left join Person on ist_autor_von.person_id=Person.person_id
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id
where Fachbeitraege_in_wissen_Zeitschriften.ver_id is not null;

--davon Kurzbeiträge
select count(*) from Wissenschaftliche_Veroeffentlichung
left join kurzbeitraege on Wissenschaftliche_Veroeffentlichung.ver_id=kurzbeitraege.ver_id
left join ist_autor_von on Wissenschaftliche_Veroeffentlichung.ver_id=ist_autor_von.ver_id
left join Person on ist_autor_von.person_id=Person.person_id
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id
where kurzbeitraege.ver_id is not null;

--davon Veröffentlichungen auf wiss. Veranstaltungen
select count(*) from Wissenschaftliche_Veroeffentlichung
left join veroeffentlichungen_im_Rahmen_von_wissen_Veranstaltungen on Wissenschaftliche_Veroeffentlichung.ver_id=veroeffentlichungen_im_Rahmen_von_wissen_Veranstaltungen.ver_id
left join ist_autor_von on Wissenschaftliche_Veroeffentlichung.ver_id=ist_autor_von.ver_id
left join Person on ist_autor_von.person_id=Person.person_id
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id
where veroeffentlichungen_im_Rahmen_von_wissen_Veranstaltungen.ver_id is not null;

-- davon sonstige Veröffentlichungen
select count(*) from Wissenschaftliche_Veroeffentlichung
left join Fachbeitraege_in_wissen_Zeitschriften on Wissenschaftliche_Veroeffentlichung.ver_id=Fachbeitraege_in_wissen_Zeitschriften.ver_id
left join veroeffentlichungen_im_Rahmen_von_wissen_Veranstaltungen on Wissenschaftliche_Veroeffentlichung.ver_id=veroeffentlichungen_im_Rahmen_von_wissen_Veranstaltungen.ver_id
left join kurzbeitraege on Wissenschaftliche_Veroeffentlichung.ver_id=kurzbeitraege.ver_id
left join Monografien_Beitraege_in_Sammelbaenden on Wissenschaftliche_Veroeffentlichung.ver_id=Monografien_Beitraege_in_Sammelbaenden.ver_id
left join ist_autor_von on Wissenschaftliche_Veroeffentlichung.ver_id=ist_autor_von.ver_id
left join Person on ist_autor_von.person_id=Person.person_id
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id
where Fachbeitraege_in_wissen_Zeitschriften.ver_id is null and veroeffentlichungen_im_Rahmen_von_wissen_Veranstaltungen.ver_id is null and kurzbeitraege.ver_id is null and Monografien_Beitraege_in_Sammelbaenden.ver_id is null;


--FuE-Projekte
select count(*) from fue_projekte
left join Professoren on fue_projekte.person_id=Professoren.person_id
left join Person on Professoren.person_id=Person.person_id
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id

--mit Kooperationspartner
select count(*) from fue_projekte
left join Professoren on fue_projekte.person_id=Professoren.person_id
left join Person on Professoren.person_id=Person.person_id
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id
left join ist_koop_von_proj on fue_projekte.fue_id=ist_koop_von_proj.fue_id
where ist_koop_von_proj.fue_id is not null;

--ohne Kooperationspartner
select count(*) from fue_projekte
left join Professoren on fue_projekte.person_id=Professoren.person_id
left join Person on Professoren.person_id=Person.person_id
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id
left join ist_koop_von_proj on fue_projekte.fue_id=ist_koop_von_proj.fue_id
where ist_koop_von_proj.fue_id is null;


--Wissenschafltiche Vorträge
select count(distinct wissenschaftliche_vortraege.vortr_id) from wissenschaftliche_vortraege
left join haelt on wissenschaftliche_vortraege.vortr_id=haelt.vortr_id
left join Person on haelt.person_id=Person.person_id
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id;

--davon gehalten auf wissen. Veranstaltungen
select count(distinct wissenschaftliche_vortraege.vortr_id) from wissenschaftliche_vortraege
left join haelt on wissenschaftliche_vortraege.vortr_id=haelt.vortr_id
left join Person on haelt.person_id=Person.person_id
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id
left join gehalten_im_Rahmen_von on wissenschaftliche_vortraege.vortr_id=gehalten_im_Rahmen_von.vortr_id
where gehalten_im_Rahmen_von.vortr_id is not null;

--davon sonstige Vorträge
select count(distinct wissenschaftliche_vortraege.vortr_id) from wissenschaftliche_vortraege
left join haelt on wissenschaftliche_vortraege.vortr_id=haelt.vortr_id
left join Person on haelt.person_id=Person.person_id
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id
left join gehalten_im_Rahmen_von on wissenschaftliche_vortraege.vortr_id=gehalten_im_Rahmen_von.vortr_id
where gehalten_im_Rahmen_von.vortr_id is null;


--Existenzgründung
select * from Existenzgruendung
left join gruendet_aus on Existenzgruendung.ex_id=gruendet_aus.ex_id
left join Person on gruendet_aus.person_id=Person.person_id
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id;


--Anmeldung von Schutzrechten
select count(*) from Schutzrechte
left join Person on Schutzrechte.person_id=Person.person_id
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id;


--Wissenschaftliche Veranstaltungen
--Fachtagungen hochschulintern
select count(*) from Wissenschaftliche_Veranstaltung
left join beteiligt_an on Wissenschaftliche_Veranstaltung.wissen_v_id=beteiligt_an.wissen_v_id
left join Organisationseinheit on beteiligt_an.org_id=Organisationseinheit.org_id
where beteiligt_an.org_id is not null and Wissenschaftliche_Veranstaltung.art_der_veranstaltung='Fachtagung';

--Fachtagung hochschulextern
select count(*) from Wissenschaftliche_Veranstaltung
left join beteiligt_an on Wissenschaftliche_Veranstaltung.wissen_v_id=beteiligt_an.wissen_v_id
left join Organisationseinheit on beteiligt_an.org_id=Organisationseinheit.org_id
where beteiligt_an.org_id is null and Wissenschaftliche_Veranstaltung.art_der_veranstaltung='Fachtagung';

--Fachmessen hochschulintern
select count(*) from Wissenschaftliche_Veranstaltung
left join beteiligt_an on Wissenschaftliche_Veranstaltung.wissen_v_id=beteiligt_an.wissen_v_id
left join Organisationseinheit on beteiligt_an.org_id=Organisationseinheit.org_id
where beteiligt_an.org_id is not null and Wissenschaftliche_Veranstaltung.art_der_veranstaltung='Fachmesse';

--Fachmessen hochschulextern
select count(*) from Wissenschaftliche_Veranstaltung
left join beteiligt_an on Wissenschaftliche_Veranstaltung.wissen_v_id=beteiligt_an.wissen_v_id
left join Organisationseinheit on beteiligt_an.org_id=Organisationseinheit.org_id
where beteiligt_an.org_id is null and Wissenschaftliche_Veranstaltung.art_der_veranstaltung='Fachmesse';

--sonstige Veranstaltungen hochschulintern
select count(*) from Wissenschaftliche_Veranstaltung
left join beteiligt_an on Wissenschaftliche_Veranstaltung.wissen_v_id=beteiligt_an.wissen_v_id
left join Organisationseinheit on beteiligt_an.org_id=Organisationseinheit.org_id
where beteiligt_an.org_id is not null and Wissenschaftliche_Veranstaltung.art_der_veranstaltung='sonstige Veranstaltung';

--sonstige Veranstaltungen hochschulextern
select count(*) from Wissenschaftliche_Veranstaltung
left join beteiligt_an on Wissenschaftliche_Veranstaltung.wissen_v_id=beteiligt_an.wissen_v_id
left join Organisationseinheit on beteiligt_an.org_id=Organisationseinheit.org_id
where beteiligt_an.org_id is null and Wissenschaftliche_Veranstaltung.art_der_veranstaltung='sonstige Veranstaltung';

--Auszeichnungen und Forschungspreise
select count(*) from auszeichnung 
left join Person on auszeichnung.person_id=Person.person_id
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id;

--im Inland
select count(*) from auszeichnung 
left join Person on auszeichnung.person_id=Person.person_id
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id
where auszeichnung.land='Deutschland';

--im Ausland
select count(*) from auszeichnung 
left join Person on auszeichnung.person_id=Person.person_id
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id
where auszeichnung.land!='Deutschland';


--Mitarbeit in Gremien
select count(*) from Gremientaetigkeit
left join Professoren on Gremientaetigkeit.person_id=Professoren.person_id
left join Person on Professoren.person_id=Person.person_id
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id

--im Inland 
select count(*) from Gremientaetigkeit
left join Professoren on Gremientaetigkeit.person_id=Professoren.person_id
left join Person on Professoren.person_id=Person.person_id
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id
where Gremientaetigkeit.land='Deutschland';

--im Ausland
select count(*) from Gremientaetigkeit
left join Professoren on Gremientaetigkeit.person_id=Professoren.person_id
left join Person on Professoren.person_id=Person.person_id
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id
where Gremientaetigkeit.land!='Deutschland';


--Gastwissenschaftler

--entsendete Gastwissenschaftler
select count(*) from gastwissenschaftlertaetigkeit
left join Mitarbeiter on gastwissenschaftlertaetigkeit.person_id_ma=Mitarbeiter.person_id
left join Professoren on gastwissenschaftlertaetigkeit.person_id_prof=Professoren.person_id
left join Person on Mitarbeiter.person_id=Person.person_id or Professoren.person_id=Person.person_id 
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id;


--aufgenommene Gastwissenschaftler
select count(*) from aufgenommene_gastwissenschaftler
left join Professoren on aufgenommene_gastwissenschaftler.person_id=Professoren.person_id
left join Person on Professoren.person_id=Person.person_id
left join gehoert_zu on Person.person_id=gehoert_zu.person_id
left join Organisationseinheit on gehoert_zu.org_id=Organisationseinheit.org_id

--Sequenzen

create sequence af_id_seq as int 
			start with 6 
			increment by 1
			no cycle;

create sequence agw_id_seq as int 
			start with 4 
			increment by 1
			no cycle;

create sequence ex_id_seq as int 
			start with 6 
			increment by 1
			no cycle;

create sequence fue_id_seq as int 
			start with 6 
			increment by 1
			no cycle;

create sequence gw_id_seq as int 
			start with 5 
			increment by 1
			no cycle;

create sequence grad_id_seq as int 
			start with 8 
			increment by 1
			no cycle;

create sequence mwgg_id_seq as int 
			start with 8 
			increment by 1
			no cycle;

create sequence koop_id_seq as int 
			start with 8 
			increment by 1
			no cycle;

create sequence org_id_seq as int 
			start with 8 
			increment by 1
			no cycle;

create sequence person_id_seq as int 
			start with 30 
			increment by 1
			no cycle;

create sequence pro_id_seq as int 
			start with 9 
			increment by 1
			no cycle;

create sequence s_id_seq as int 
			start with 6 
			increment by 1
			no cycle;

create sequence wissen_v_id_seq as int 
			start with 12 
			increment by 1
			no cycle;

create sequence ver_id_seq as int 
			start with 7 
			increment by 1
			no cycle;

create sequence vortr_id_seq as int 
			start with 6 
			increment by 1
			no cycle;
